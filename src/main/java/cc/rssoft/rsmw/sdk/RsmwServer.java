package cc.rssoft.rsmw.sdk;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;

import cc.rssoft.rsmw.sdk.command.AccessNumberDeleteCommand;
import cc.rssoft.rsmw.sdk.command.AccessNumberGatewayLinkCommand;
import cc.rssoft.rsmw.sdk.command.AccessNumberGetAllCommand;
import cc.rssoft.rsmw.sdk.command.AccessNumberRouteDeleteCommand;
import cc.rssoft.rsmw.sdk.command.AccessNumberRouteGetAllCommand;
import cc.rssoft.rsmw.sdk.command.AccessNumberRouteSaveCommand;
import cc.rssoft.rsmw.sdk.command.AccessNumberRouteUpdateCommand;
import cc.rssoft.rsmw.sdk.command.AccessNumberSaveCommand;
import cc.rssoft.rsmw.sdk.command.AccessNumberUpdateCommand;
import cc.rssoft.rsmw.sdk.command.AgentDeleteCommand;
import cc.rssoft.rsmw.sdk.command.AgentGetAllCommand;
import cc.rssoft.rsmw.sdk.command.AgentSaveCommand;
import cc.rssoft.rsmw.sdk.command.AgentSyncCommand;
import cc.rssoft.rsmw.sdk.command.BindCommand;
import cc.rssoft.rsmw.sdk.command.BlacklistDeleteCommand;
import cc.rssoft.rsmw.sdk.command.BlacklistGetAllCommand;
import cc.rssoft.rsmw.sdk.command.BlacklistSaveCommand;
import cc.rssoft.rsmw.sdk.command.BridgeCommand;
import cc.rssoft.rsmw.sdk.command.ChanSpyCommand;
import cc.rssoft.rsmw.sdk.command.ConferenceInviteCommand;
import cc.rssoft.rsmw.sdk.command.ConferenceKickCommand;
import cc.rssoft.rsmw.sdk.command.ConferenceLockCommand;
import cc.rssoft.rsmw.sdk.command.ConferenceMuteCommand;
import cc.rssoft.rsmw.sdk.command.ConferenceReInviteCommand;
import cc.rssoft.rsmw.sdk.command.ConferenceUnlockCommand;
import cc.rssoft.rsmw.sdk.command.ConferenceUnmuteCommand;
import cc.rssoft.rsmw.sdk.command.GatewayDeleteCommand;
import cc.rssoft.rsmw.sdk.command.GatewayGetAllCommand;
import cc.rssoft.rsmw.sdk.command.GatewaySaveCommand;
import cc.rssoft.rsmw.sdk.command.GatewayUpdateCommand;
import cc.rssoft.rsmw.sdk.command.GetBindInfoCommand;
import cc.rssoft.rsmw.sdk.command.GetCdrByCallUuidCommand;
import cc.rssoft.rsmw.sdk.command.GetCdrByUniqueidCommand;
import cc.rssoft.rsmw.sdk.command.GetChannelCommand;
import cc.rssoft.rsmw.sdk.command.GetChannelCountCommand;
import cc.rssoft.rsmw.sdk.command.GetChannelVariablesCommand;
import cc.rssoft.rsmw.sdk.command.GetConferenceStatusCommand;
import cc.rssoft.rsmw.sdk.command.GetGatewayStatusCommand;
import cc.rssoft.rsmw.sdk.command.GetMobileInfoCommand;
import cc.rssoft.rsmw.sdk.command.GetQueueStatusCommand;
import cc.rssoft.rsmw.sdk.command.GetSipPhoneStatusCommand;
import cc.rssoft.rsmw.sdk.command.HangupCommand;
import cc.rssoft.rsmw.sdk.command.HoldCommand;
import cc.rssoft.rsmw.sdk.command.IsExtenCommand;
import cc.rssoft.rsmw.sdk.command.IsPhoneNumberBlockInCommand;
import cc.rssoft.rsmw.sdk.command.IsPhoneNumberBlockOutCommand;
import cc.rssoft.rsmw.sdk.command.OriginateCommand;
import cc.rssoft.rsmw.sdk.command.QueueDeleteCommand;
import cc.rssoft.rsmw.sdk.command.QueueGetAllCommand;
import cc.rssoft.rsmw.sdk.command.QueueMemberDeleteOutboundNumberCommand;
import cc.rssoft.rsmw.sdk.command.QueueMemberPauseCommand;
import cc.rssoft.rsmw.sdk.command.QueueMemberSaveOutboundNumberCommand;
import cc.rssoft.rsmw.sdk.command.QueueMemberSetSipPhoneOrAgentCommand;
import cc.rssoft.rsmw.sdk.command.QueueMemberUnPauseCommand;
import cc.rssoft.rsmw.sdk.command.QueueSaveCommand;
import cc.rssoft.rsmw.sdk.command.QueueUpdateCommand;
import cc.rssoft.rsmw.sdk.command.RedirectCommand;
import cc.rssoft.rsmw.sdk.command.SipPhoneDeleteCommand;
import cc.rssoft.rsmw.sdk.command.SipPhoneGetAllCommand;
import cc.rssoft.rsmw.sdk.command.SipPhoneSaveCommand;
import cc.rssoft.rsmw.sdk.command.SipPhoneUpdateCommand;
import cc.rssoft.rsmw.sdk.command.SqeCommand;
import cc.rssoft.rsmw.sdk.command.UnBindCommand;
import cc.rssoft.rsmw.sdk.command.UnHoldCommand;
import cc.rssoft.rsmw.sdk.command.VoiceVerificationCodeCommand;
import cc.rssoft.rsmw.sdk.command.bean.AccessNumber;
import cc.rssoft.rsmw.sdk.command.bean.AccessNumberRoute;
import cc.rssoft.rsmw.sdk.command.bean.Agent;
import cc.rssoft.rsmw.sdk.command.bean.Blacklist;
import cc.rssoft.rsmw.sdk.command.bean.Gateway;
import cc.rssoft.rsmw.sdk.command.bean.Queue;
import cc.rssoft.rsmw.sdk.command.bean.SipPhone;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;
import cc.rssoft.rsmw.sdk.command.internal.SimpleResponse;
import cc.rssoft.rsmw.sdk.command.response.AccessNumberGetAllResponse;
import cc.rssoft.rsmw.sdk.command.response.AccessNumberRouteGetAllResponse;
import cc.rssoft.rsmw.sdk.command.response.AgentGetAllResponse;
import cc.rssoft.rsmw.sdk.command.response.AgentSipPhoneBindInfoResponse;
import cc.rssoft.rsmw.sdk.command.response.BlacklistGetAllResponse;
import cc.rssoft.rsmw.sdk.command.response.GatewayGetAllResponse;
import cc.rssoft.rsmw.sdk.command.response.GetCdrByCallUuidResponse;
import cc.rssoft.rsmw.sdk.command.response.GetCdrByUniqueidResponse;
import cc.rssoft.rsmw.sdk.command.response.GetChannelCountResponse;
import cc.rssoft.rsmw.sdk.command.response.GetChannelResponse;
import cc.rssoft.rsmw.sdk.command.response.GetChannelVariablesResponse;
import cc.rssoft.rsmw.sdk.command.response.GetConferenceStatusResponse;
import cc.rssoft.rsmw.sdk.command.response.GetGatewayStatusResponse;
import cc.rssoft.rsmw.sdk.command.response.GetMobileInfoResponse;
import cc.rssoft.rsmw.sdk.command.response.GetQueueStatusResponse;
import cc.rssoft.rsmw.sdk.command.response.GetSipPhoneStatusResponse;
import cc.rssoft.rsmw.sdk.command.response.IsExtenResponse;
import cc.rssoft.rsmw.sdk.command.response.IsPhoneNumberBlockInResponse;
import cc.rssoft.rsmw.sdk.command.response.IsPhoneNumberBlockOutResponse;
import cc.rssoft.rsmw.sdk.command.response.OriginateResponse;
import cc.rssoft.rsmw.sdk.command.response.QueueGetAllResponse;
import cc.rssoft.rsmw.sdk.command.response.QueueMemberPauseResponse;
import cc.rssoft.rsmw.sdk.command.response.SipPhoneGetAllResponse;
import cc.rssoft.rsmw.sdk.command.response.VoiceVerificationCodeResponse;
import cc.rssoft.rsmw.sdk.util.RsmwSignUtil;

public class RsmwServer {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private String rsmwUrl;
	private String apiSecret;
	private int connectTimeout = 2000;
	private int readTimeout = 2000;

	private SSLContext sslContext = null;

	public RsmwServer() {
		try {
			sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, trustAllCerts, new SecureRandom());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	public RsmwServer(String rsmwUrl, String apiSecret, int connectTimeout, int readTimeout) {
		this.rsmwUrl = rsmwUrl;
		this.apiSecret = apiSecret;
		this.connectTimeout = connectTimeout;
		this.readTimeout = readTimeout;
		try {
			sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, trustAllCerts, new SecureRandom());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	public <T extends AbstractApiResponse> T sendCommand(AbstractApiCommand apiCommand, Class<T> clazz) {

		T t = null;

		try {

			String commandString = apiCommand.toJsonString();
			logger.debug("commandString=" + commandString);

			String urlString = rsmwUrl + "?sign=" + RsmwSignUtil.sign(commandString, apiSecret);
			logger.debug("request url=" + urlString);
			logger.debug("request body=" + commandString);
			URL url = new URL(urlString);

			HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);
			conn.setDoOutput(true);
			conn.connect();

			// send http request
			OutputStream outStream = conn.getOutputStream();
			outStream.write(commandString.getBytes());
			outStream.flush();
			outStream.close();

			// receive http response
			StringBuilder result = new StringBuilder();
			BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line = null;
			while ((line = r.readLine()) != null) {
				result.append(line);
			}
			t = JSON.parseObject(result.toString(), clazz);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return t;
	}

	// private <T extends AbstractApiResponse> T sendCommand(AbstractApiCommand
	// apiCommand, Class<T> clazz) {
	// String commandString = apiCommand.toJsonString();
	// logger.debug("commandString="+commandString);
	// String url = rsmwUrl + "?sign=" + RsmwSignUtil.sign(commandString,
	// apiSecret);
	// logger.debug("request url="+url);
	// logger.debug("request body="+commandString);
	// HttpRequest httpRequest =
	// HttpRequest.post(url).contentType("application/json;charset=UTF-8")
	// .connectTimeout(connectTimeout).readTimeout(readTimeout).send(commandString);
	// httpRequest.trustAllCerts();
	// String body = httpRequest.body();
	// logger.debug("RESPONSE BODY="+body);
	// T t = JSON.parseObject(body, clazz);
	//
	// return t;
	// }

	public String getRsmwUrl() {
		return rsmwUrl;
	}

	public void setRsmwUrl(String rsmwUrl) {
		this.rsmwUrl = rsmwUrl;
	}

	public String getApiSecret() {
		return apiSecret;
	}

	public void setApiSecret(String apiSecret) {
		this.apiSecret = apiSecret;
	}

	public int getConnectTimeout() {
		return connectTimeout;
	}

	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	public int getReadTimeout() {
		return readTimeout;
	}

	public void setReadTimeout(int readTimeout) {
		this.readTimeout = readTimeout;
	}

	/**
	 * 5.1 【新增Agent】<br>
	 * 
	 * @param agentAid
	 *            第三方应用中用户的唯一标识，一般是id或用户名
	 * @param agentName
	 *            姓名
	 * @param agentJobNumber
	 *            工号
	 * @return simpleResponse
	 */
	public SimpleResponse agentSave(Agent agent) {
		AgentSaveCommand request = new AgentSaveCommand(agent);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 5.1 【删除Agent】<br>
	 * 
	 * @param agentAidList
	 *            关于agentAid的list集合
	 * @return simpleResponse
	 */
	public SimpleResponse agentDelete(List<String> agentAidList) {
		AgentDeleteCommand request = new AgentDeleteCommand(agentAidList);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 5.1 【同步Agent】<br>
	 * 
	 * @param agentAidList
	 * @return
	 */
	public SimpleResponse agentSync(List<Agent> agentList) {
		AgentSyncCommand request = new AgentSyncCommand(agentList);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 5.1 【获取所有Agent】<br>
	 * 
	 * @return agentListAllResponse
	 */
	public AgentGetAllResponse agentGetAll() {
		AgentGetAllCommand request = new AgentGetAllCommand();
		AgentGetAllResponse response = this.sendCommand(request, AgentGetAllResponse.class);
		return response;
	}

	/**
	 * 5.2 【绑定】<br>
	 * 用途：将Agent和SipPhone建立“一对一对的应关系”告诉rsmw“哪个Agent在使用哪个SipPhone”。<br>
	 * 调用时机：在用户登录时调用【绑定】<br>
	 * 
	 * @param agentAid
	 *            第三方应用中用户的唯一标识，一般是id或用户名
	 * @param sipPhoneName
	 *            所要绑定的SIPPHONE的分机号
	 * @return simpleResponse
	 */
	public SimpleResponse bind(String agentAid, String sipPhoneName) {
		BindCommand request = new BindCommand(agentAid, sipPhoneName);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 5.2 【解绑】<br>
	 * 用途：将Agent和SipPhone建立“一对一对的应关系”告诉rsmw“哪个Agent在使用哪个SipPhone”。<br>
	 * 调用时机：在用户注销时调用【解绑】<br>
	 * 
	 * @param agentAid
	 *            第三方应用中用户的唯一标识，一般是id或用户名
	 * @param sipPhoneName
	 *            所要绑定的SIPPHONE的分机号
	 * @return simpleResponse
	 */
	public SimpleResponse unBind(String agentAid, String sipPhoneName) {

		UnBindCommand request = new UnBindCommand(agentAid, sipPhoneName);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);

		return response;
	}

	/**
	 * 5.3 【绑定关系查询】<br>
	 * 用途：查询Agent和SipPhone的绑定关系 <br>
	 * 
	 * @param agentAid
	 *            第三方应用中用户的唯一标识，一般是id或用户名
	 * @param sipPhoneName
	 *            所要绑定的SIPPHONE的分机号
	 * @return agentSipPhoneBindInfoResponse
	 */
	public AgentSipPhoneBindInfoResponse getBindInfo(String agentAid, String sipPhoneName) {

		GetBindInfoCommand request = new GetBindInfoCommand(agentAid, sipPhoneName);
		AgentSipPhoneBindInfoResponse response = this.sendCommand(request, AgentSipPhoneBindInfoResponse.class);

		return response;
	}

	/**
	 * 5.4 【置忙】 <br>
	 * 用途：将Agent的状态设置为“忙” <br>
	 * 调用时机：Agent在界面上点击“置忙”的按钮或链接时调用。 <br>
	 * 效果：处于“忙”状态的Agent将不会接到“技能组”分配的呼入电话 <br>
	 * 
	 * @param queueMemberInterface
	 *            技能组成员所使用的设备拨号字符串，格式为：SIP/${sipPhoneName}
	 * @param reason
	 *            置忙原因
	 * @return simpleResponse
	 */
	public QueueMemberPauseResponse queueMemberPause(String queueMemberInterface, String reason) {
		QueueMemberPauseCommand request = new QueueMemberPauseCommand(queueMemberInterface, reason);
		QueueMemberPauseResponse response = this.sendCommand(request, QueueMemberPauseResponse.class);
		return response;
	}

	/**
	 * 5.4 【置闲】<br>
	 * 用途：将Agent的状态设置为“闲” <br>
	 * 调用时机：Agent在界面上点击“置闲”的按钮或链接时调用。 <br>
	 * 效果：处于“忙”状态的Agent将不会接到“技能组”分配的呼入电话 <br>
	 * 
	 * @param queueMemberInterface
	 *            技能组成员所使用的设备拨号字符串，格式为：SIP/${sipPhoneName}
	 * @return simpleResponse
	 */
	public QueueMemberPauseResponse queueMemberUnPause(String queueMemberInterface) {
		QueueMemberUnPauseCommand request = new QueueMemberUnPauseCommand(queueMemberInterface);
		QueueMemberPauseResponse response = this.sendCommand(request, QueueMemberPauseResponse.class);
		return response;
	}

	/**
	 * 5.5 【实时并发数查询】<br>
	 * 用途：查询系统当前的并发通话Channe的总数<br>
	 * 调用时机：一般在发起呼叫前调用，用来判断系统的负载，决定是否要发起呼叫<br>
	 * 
	 * @return getChannelCountResponse
	 */
	public GetChannelCountResponse getChannelCount() {
		GetChannelCountCommand request = new GetChannelCountCommand();
		GetChannelCountResponse response = this.sendCommand(request, GetChannelCountResponse.class);
		return response;
	}

	/**
	 * 5.6 【发起呼叫】<br>
	 * 
	 * @param absoluteTimeout
	 *            通话超时时间（秒）。超时后自动挂断。0为永不超时
	 * @param src
	 *            第一路呼叫的号码
	 * @param srcGateway
	 *            第一路呼叫所需要走的gateway。若src为内部分机号，则此字段无需填写
	 * @param srcAccessNumber
	 *            第一路呼叫所需要用的主叫号。若src为内部分机号，则此字段无需填写
	 * @param srcCallerIdName
	 *            第一路呼叫所需要用的主叫（显示用）
	 * @param srcCallerIdNumber
	 *            第一路呼叫所需要用的主叫（显示用）
	 * @param srcAnnounceMediaUrl
	 *            第一路呼叫接通后所听到的提示音
	 * @param dst
	 *            第二路呼叫的号码
	 * @param dstGateway
	 *            第二路呼叫所需要走的gateway。若dst为内部分机号，则此字段无需填写
	 * @param dstAccessNumber
	 *            第二路呼叫所需要用的主叫号。若dst为内部分机号，则此字段无需填写
	 * @param dstAnnounceMediaUrl
	 *            第二路呼叫接通后所听到的提示音
	 * @param userData
	 *            自定义数据。会随CdrEvent返回
	 * @return originateResponse
	 */
	public OriginateResponse originate(Integer absoluteTimeout, String src, String srcGateway,
			String srcAccessNumber, String srcCallerIdName, String srcCallerIdNumber, String srcAnnounceMediaUrl,
			String dst, String dstGateway, String dstAccessNumber, String dstAnnounceMediaUrl, String userData) {
		OriginateCommand request = new OriginateCommand(absoluteTimeout, src, srcGateway, srcAccessNumber,
				srcCallerIdName, srcCallerIdNumber, srcAnnounceMediaUrl, dst, dstGateway, dstAccessNumber,
				dstAnnounceMediaUrl, userData);
		OriginateResponse response = this.sendCommand(request, OriginateResponse.class);
		return response;
	}

	/**
	 * 发起呼叫 适用于不显示主叫号和呼叫不带自定义数据。
	 * @param absoluteTimeout
     *            通话超时时间（秒）。超时后自动挂断。0为永不超时
     * @param src
     *            第一路呼叫的号码
     * @param srcGateway
     *            第一路呼叫所需要走的gateway。若src为内部分机号，则此字段无需填写
     * @param srcAccessNumber
     *            第一路呼叫所需要用的主叫号。若src为内部分机号，则此字段无需填写
     * @param srcAnnounceMediaUrl
     *            第一路呼叫接通后所听到的提示音
     * @param dst
     *            第二路呼叫的号码
     * @param dstGateway
     *            第二路呼叫所需要走的gateway。若dst为内部分机号，则此字段无需填写
     * @param dstAccessNumber
     *            第二路呼叫所需要用的主叫号。若dst为内部分机号，则此字段无需填写
     * @param dstAnnounceMediaUrl
     *            第二路呼叫接通后所听到的提示音
     * @return originateResponse
	 */
	public OriginateResponse originate(Integer absoluteTimeout, String src, String srcGateway,
			String srcAccessNumber, String srcAnnounceMediaUrl, String dst, String dstGateway, String dstAccessNumber,
			String dstAnnounceMediaUrl) {
		return originate(absoluteTimeout, src, srcGateway, srcAccessNumber, "", "", srcAnnounceMediaUrl, dst,
				dstGateway, dstAccessNumber, dstAnnounceMediaUrl, "");
	}

	/**
	 * 发起呼叫
	 * 适用于两个分机号互打
	 * @param src 主叫分机号
	 * @param dst 被叫分机号
	 * @return
	 */
	public OriginateResponse originate(String src, String dst) {
		return originate(0, src, "", "", "", "", "", dst, "", "", "", "");
	}

	/**
	 * 5.7 【获取指定SipPhone的当前Channel】<br>
	 * 用途：获取sipPhone的channel以及正在和该sipPhone通话的channel<br>
	 * 
	 * @param sipPhoneName
	 *            sipPhone的分机号
	 * @return getChannelResponse
	 */
	public GetChannelResponse getChannel(String sipPhoneName) {
		GetChannelCommand request = new GetChannelCommand(sipPhoneName);
		GetChannelResponse response = this.sendCommand(request, GetChannelResponse.class);
		return response;
	}

	/**
	 * 5.8 【呼叫保持、恢复】<br>
	 * 
	 * @param channel
	 *            希望挂断的channel名称
	 * @return simpleResponse
	 */
	public SimpleResponse hold(String sipPhoneName) {
		HoldCommand request = new HoldCommand(sipPhoneName);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 5.8 【呼叫保持、恢复】<br>
	 * 
	 * @param channel
	 *            希望挂断的channel名称
	 * @return simpleResponse
	 */
	public SimpleResponse unhold(String sipPhoneName) {
		UnHoldCommand request = new UnHoldCommand(sipPhoneName);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 5.9 【桥接通话】
	 * 
	 * @param channel1
	 * @param channel2
	 * @return
	 */
	public SimpleResponse bridge(String channel1, String channel2) {
		BridgeCommand request = new BridgeCommand(channel1, channel2);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 5.10 【转接Channel】<br>
	 * 用途：将制定的通话转接到指定的exten<br>
	 * 
	 * @param channel
	 *            希望转接的channel名称
	 * @param exten
	 *            希望将channel转接到的目标分机号
	 * @return simpleResponse
	 */
	public SimpleResponse redirect(String channel, String context, String exten) {
		return redirect(channel, context, exten, "", "", "");
	}

	/**
	 * 5.10 【转接Channel】<br>
	 * 用途：将制定的通话转接到指定的exten<br>
	 * 
	 * @param channel
	 * @param exten
	 * @param extraChannel
	 * @param extraExten
	 * @return
	 */
	public SimpleResponse redirect(String channel, String context, String exten, String extraContext,
			String extraChannel, String extraExten) {
		return redirect(channel, context, exten, "", "", extraChannel, extraContext, extraExten, "", "");
	}

	/**
	 * 5.10 【转接Channel】<br>
	 * 用途：将制定的通话转接到指定的exten<br>
	 * 
	 * @param channel
	 * @param exten
	 * @param dstGateway
	 * @param dstAccessNumber
	 * @param extraChannel
	 * @param extraExten
	 * @param extraDstGateway
	 * @param extraDstAccessNumber
	 * @return
	 */
	public SimpleResponse redirect(String channel, String context, String exten, String dstGateway,
			String dstAccessNumber, String extraChannel, String extraContext, String extraExten, String extraDstGateway,
			String extraDstAccessNumber) {
		RedirectCommand request = new RedirectCommand(channel, context, exten, dstGateway, dstAccessNumber,
				extraChannel, extraContext, extraExten, extraDstGateway, extraDstAccessNumber);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 5.11 【挂断Channel】<br>
	 * 
	 * @param channel
	 *            希望挂断的channel名称
	 * @return simpleResponse
	 */
	public SimpleResponse hangup(String channel) {
		HangupCommand request = new HangupCommand(channel);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 5.12 【获取所有SipPhone状态】<br>
	 * 
	 * @return getSipPhoneStatusResponse
	 */
	public GetSipPhoneStatusResponse getSipPhoneStatus() {
		GetSipPhoneStatusCommand request = new GetSipPhoneStatusCommand();
		GetSipPhoneStatusResponse response = this.sendCommand(request, GetSipPhoneStatusResponse.class);
		return response;
	}

	/**
	 * 5.13 【获取所有Gateway状态】
	 * 
	 * @return
	 */
	public GetGatewayStatusResponse getGatewayStatus() {
		GetGatewayStatusCommand request = new GetGatewayStatusCommand();
		GetGatewayStatusResponse response = this.sendCommand(request, GetGatewayStatusResponse.class);
		return response;
	}

	/**
	 * 5.14 【获取所有技能组状态】<br>
	 * 
	 * @return getQueueStatusResponse
	 */
	public GetQueueStatusResponse getQueueStatus() {
		GetQueueStatusCommand request = new GetQueueStatusCommand();
		GetQueueStatusResponse response = this.sendCommand(request, GetQueueStatusResponse.class);
		return response;
	}

	/**
	 * 5.15 【查询归属地】<br>
	 * 
	 * @param mobilePhoneNumber
	 * @return getMobileInfoResponse
	 */
	public GetMobileInfoResponse getMobileInfo(String mobilePhoneNumber) {
		GetMobileInfoCommand request = new GetMobileInfoCommand(mobilePhoneNumber);
		GetMobileInfoResponse response = this.sendCommand(request, GetMobileInfoResponse.class);
		return response;
	}

	/**
	 * 5.16 【查询Cdr】<br>
	 * 
	 * @param mobilePhoneNumber
	 * @return getMobileInfoResponse
	 */
	public GetCdrByUniqueidResponse getCdrByUniqueid(String uniqueid) {
		GetCdrByUniqueidCommand request = new GetCdrByUniqueidCommand(uniqueid);
		GetCdrByUniqueidResponse response = this.sendCommand(request, GetCdrByUniqueidResponse.class);
		return response;
	}

	/**
	 * 5.16 【查询Cdr】<br>
	 * 
	 * @param mobilePhoneNumber
	 * @return getMobileInfoResponse
	 */
	public GetCdrByCallUuidResponse getCdrByCallUuid(String uuid) {
		GetCdrByCallUuidCommand request = new GetCdrByCallUuidCommand(uuid);
		GetCdrByCallUuidResponse response = this.sendCommand(request, GetCdrByCallUuidResponse.class);
		return response;
	}

	/**
	 * 5.17 【服务质量评价】<br>
	 * 
	 * @param sqeSipPhoneName
	 * @param sqeQuestion
	 * @param sqeThankyou
	 * @return
	 */
	public SimpleResponse sqe(String sqeSipPhoneName, String sqeQuestion, String sqeThankyou) {
		SqeCommand request = new SqeCommand(sqeSipPhoneName, sqeQuestion, sqeThankyou);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 5.18 【判断分机号是否内部exten】<br>
	 * 
	 * @param exten
	 * @return
	 */
	public IsExtenResponse isExten(String exten) {
		IsExtenCommand request = new IsExtenCommand(exten);
		IsExtenResponse response = this.sendCommand(request, IsExtenResponse.class);
		return response;
	}

	/**
	 * 5.19 【监听】
	 * 
	 * @param spySipPhoneName
	 * @param spiedSipPhoneName
	 * @return
	 */
	public SimpleResponse chanSpy(String spySipPhoneName, String spiedSipPhoneName) {
		ChanSpyCommand chanSpyCommand = new ChanSpyCommand(spySipPhoneName, spiedSipPhoneName);
		SimpleResponse simpleResponse = this.sendCommand(chanSpyCommand, SimpleResponse.class);
		return simpleResponse;
	}

	/**
	 * 5.20 【语音验证码】
	 * 
	 * @return
	 */
	public VoiceVerificationCodeResponse voiceVerificationCode(String phoneNumber, String gateway,
			String accessNumber, String exten, String media1, String code, String media2, Integer repeat,
			Integer absoluteTimeout) {
		VoiceVerificationCodeCommand request = new VoiceVerificationCodeCommand(phoneNumber, gateway,
				accessNumber, exten, media1, code, media2, repeat, absoluteTimeout);
		VoiceVerificationCodeResponse response = this.sendCommand(request, VoiceVerificationCodeResponse.class);
		return response;
	}

	public GetChannelVariablesResponse getChannelVariables() {
		GetChannelVariablesCommand request = new GetChannelVariablesCommand();
		GetChannelVariablesResponse response = this.sendCommand(request, GetChannelVariablesResponse.class);
		return response;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 添加接入号
	 * 
	 * @param accessNumber
	 * @return
	 */
	public SimpleResponse accessNumberSave(AccessNumber accessNumber) {
		AccessNumberSaveCommand request = new AccessNumberSaveCommand(accessNumber);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 删除接入号
	 * 
	 * @param ids
	 * @return
	 */
	public SimpleResponse accessNumberDelete(List<Long> ids) {
		AccessNumberDeleteCommand request = new AccessNumberDeleteCommand(ids);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 更新接入号
	 * 
	 * @param accessNumber
	 * @return
	 */
	public SimpleResponse accessNumberUpdate(AccessNumber accessNumber) {
		AccessNumberUpdateCommand request = new AccessNumberUpdateCommand(accessNumber);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 获取接入号
	 * 
	 * @return
	 */
	public AccessNumberGetAllResponse accessNumberGetAll() {
		AccessNumberGetAllCommand apiCommand = new AccessNumberGetAllCommand();
		AccessNumberGetAllResponse response = this.sendCommand(apiCommand, AccessNumberGetAllResponse.class);
		return response;
	}

	/**
	 * 添加网关
	 * 
	 * @param gateway
	 * @return
	 */
	public SimpleResponse gatewaySave(Gateway gateway) {
		GatewaySaveCommand request = new GatewaySaveCommand(gateway);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 删除网关
	 * 
	 * @param ids
	 * @return
	 */
	public SimpleResponse gatewayDelete(List<Long> ids) {
		GatewayDeleteCommand request = new GatewayDeleteCommand(ids);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 更新网关
	 * 
	 * @param gateway
	 * @return
	 */
	public SimpleResponse gatewayUpdate(Gateway gateway) {
		GatewayUpdateCommand request = new GatewayUpdateCommand(gateway);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 获取所有网关
	 * 
	 * @return
	 */
	public GatewayGetAllResponse gatewayGetAll() {
		GatewayGetAllCommand apiCommand = new GatewayGetAllCommand();
		GatewayGetAllResponse response = this.sendCommand(apiCommand, GatewayGetAllResponse.class);
		return response;
	}

	/**
	 * 删除网关接入号对应关系
	 * 
	 * @param accessNumberId
	 * @param gatewayIdList
	 * @return
	 */
	public SimpleResponse accessNumberGatewayLink(Long accessNumberId, List<Long> gatewayIdList) {
		AccessNumberGatewayLinkCommand apiCommand = new AccessNumberGatewayLinkCommand(accessNumberId, gatewayIdList);
		SimpleResponse response = this.sendCommand(apiCommand, SimpleResponse.class);
		return response;
	}

	/**
	 * 添加分机
	 * 
	 * @param sipPhone
	 * @return
	 */
	public SimpleResponse sipPhoneSave(SipPhone sipPhone) {
		SipPhoneSaveCommand request = new SipPhoneSaveCommand(sipPhone);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 删除分机
	 * 
	 * @param ids
	 * @return
	 */
	public SimpleResponse sipPhoneDelete(List<Long> ids) {
		SipPhoneDeleteCommand request = new SipPhoneDeleteCommand(ids);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 更新分机
	 * 
	 * @param sipPhone
	 * @return
	 */
	public SimpleResponse sipPhoneUpdate(SipPhone sipPhone) {
		SipPhoneUpdateCommand request = new SipPhoneUpdateCommand(sipPhone);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 获取分机
	 * 
	 * @return
	 */
	public SipPhoneGetAllResponse sipPhoneGetAll() {
		SipPhoneGetAllCommand apiCommand = new SipPhoneGetAllCommand();
		SipPhoneGetAllResponse response = this.sendCommand(apiCommand, SipPhoneGetAllResponse.class);
		return response;
	}

	/**
	 * 保存接入路由
	 * 
	 * @param accessNumberRoute
	 * @return
	 */
	public SimpleResponse accessNumberRouteSave(AccessNumberRoute accessNumberRoute) {
		AccessNumberRouteSaveCommand request = new AccessNumberRouteSaveCommand(accessNumberRoute);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 删除接入路由
	 * 
	 * @param ids
	 * @return
	 */
	public SimpleResponse accessNumberRouteDelete(List<Long> ids) {
		AccessNumberRouteDeleteCommand request = new AccessNumberRouteDeleteCommand(ids);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 更新接入路由
	 * 
	 * @param accessNumberRoute
	 * @return
	 */
	public SimpleResponse accessNumberRouteUpdate(AccessNumberRoute accessNumberRoute) {
		AccessNumberRouteUpdateCommand request = new AccessNumberRouteUpdateCommand(accessNumberRoute);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 获取接入路由
	 * 
	 * @return
	 */
	public AccessNumberRouteGetAllResponse accessNumberRouteGetAll() {
		AccessNumberRouteGetAllCommand apiCommand = new AccessNumberRouteGetAllCommand();
		AccessNumberRouteGetAllResponse response = this.sendCommand(apiCommand, AccessNumberRouteGetAllResponse.class);
		return response;
	}

	/**
	 * 添加剂能组
	 * 
	 * @param queue
	 * @return
	 */
	public SimpleResponse queueSave(Queue queue) {
		QueueSaveCommand request = new QueueSaveCommand(queue);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 删除技能组
	 * 
	 * @param ids
	 * @return
	 */
	public SimpleResponse queueDelete(List<Long> ids) {
		QueueDeleteCommand request = new QueueDeleteCommand(ids);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 更新技能组
	 * 
	 * @param queue
	 * @return
	 */
	public SimpleResponse queueUpdate(Queue queue) {
		QueueUpdateCommand request = new QueueUpdateCommand(queue);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 获取技能组
	 * 
	 * @return
	 */
	public QueueGetAllResponse queueGetAll() {
		QueueGetAllCommand apiCommand = new QueueGetAllCommand();
		QueueGetAllResponse response = this.sendCommand(apiCommand, QueueGetAllResponse.class);
		return response;
	}

	/**
	 * 保存技能组成员接入号
	 * 
	 * @param queueId
	 * @param outboundNumber
	 * @param gatewayName
	 * @param penalty
	 * @return
	 */
	public SimpleResponse queueMemberSaveOutboundNumber(Long queueId, String outboundNumber, String gatewayName,
			Integer penalty) {
		QueueMemberSaveOutboundNumberCommand request = new QueueMemberSaveOutboundNumberCommand(queueId, outboundNumber,
				gatewayName, penalty);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 删除技能组成员的接入号
	 * 
	 * @param queueId
	 * @param outboundNumberId
	 * @return
	 */
	public SimpleResponse queueMemberDeleteOutboundNumber(Long queueId, Long outboundNumberId) {
		QueueMemberDeleteOutboundNumberCommand request = new QueueMemberDeleteOutboundNumberCommand(queueId,
				outboundNumberId);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 设置技能组的分机或座席
	 * 
	 * @param queueId
	 * @param sipPhoneIdList
	 * @param agentAidList
	 * @return
	 */
	public SimpleResponse queueMemberSetSipPhoneOrAgent(Long queueId, List<Long> sipPhoneIdList,
			List<String> agentAidList) {
		QueueMemberSetSipPhoneOrAgentCommand request = new QueueMemberSetSipPhoneOrAgentCommand(queueId, sipPhoneIdList,
				agentAidList);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 添加黑名单
	 * 
	 * @param blacklist
	 * @return
	 */
	public SimpleResponse blacklistSave(Blacklist blacklist) {
		BlacklistSaveCommand request = new BlacklistSaveCommand(blacklist);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 删除黑名单
	 * 
	 * @param ids
	 * @return
	 */
	public SimpleResponse blacklistDelete(List<Long> ids) {
		BlacklistDeleteCommand request = new BlacklistDeleteCommand(ids);
		SimpleResponse response = this.sendCommand(request, SimpleResponse.class);
		return response;
	}

	/**
	 * 所有黑名单
	 * 
	 * @return
	 */
	public BlacklistGetAllResponse blacklistGetAll() {
		BlacklistGetAllCommand apiCommand = new BlacklistGetAllCommand();
		BlacklistGetAllResponse response = this.sendCommand(apiCommand, BlacklistGetAllResponse.class);
		return response;
	}

	/**
	 * 呼出黑名单
	 * 
	 * @param phoneNumber
	 * @return
	 */
	public IsPhoneNumberBlockOutResponse isPhoneNumberBlockOut(String phoneNumber) {
		IsPhoneNumberBlockOutCommand apiCommand = new IsPhoneNumberBlockOutCommand(phoneNumber);
		IsPhoneNumberBlockOutResponse response = this.sendCommand(apiCommand, IsPhoneNumberBlockOutResponse.class);
		return response;
	}

	/**
	 * 呼入黑名单
	 * 
	 * @param phoneNumber
	 * @return
	 */
	public IsPhoneNumberBlockInResponse isPhoneNumberBlockIn(String phoneNumber) {
		IsPhoneNumberBlockInCommand apiCommand = new IsPhoneNumberBlockInCommand(phoneNumber);
		IsPhoneNumberBlockInResponse response = this.sendCommand(apiCommand, IsPhoneNumberBlockInResponse.class);
		return response;
	}

	/**
	 * 查询会议的状态
	 * 
	 * @return
	 */
	public GetConferenceStatusResponse getConferenceStatus(String exten) {
		GetConferenceStatusCommand apiCommand = new GetConferenceStatusCommand(exten);
		GetConferenceStatusResponse response = this.sendCommand(apiCommand, GetConferenceStatusResponse.class);
		return response;
	}

	/**
	 * 会议室成员邀请
	 * @param exten
	 * @param gateway
	 * @param accessNumber
	 * @param phoneNumberList
	 * @return
	 */
	public SimpleResponse conferenceInvite(String exten,String gateway, String accessNumber,List<String> phoneNumberList) {
		ConferenceInviteCommand apiCommand = new ConferenceInviteCommand( exten, gateway,  accessNumber, phoneNumberList);
		SimpleResponse response = this.sendCommand(apiCommand, SimpleResponse.class);
		return response;
	}
	
	/**
	 * 会议室成员重新邀请
	 * 
	 * @return
	 */
	public SimpleResponse conferenceReInvite(String exten, String phoneNUmber) {
		ConferenceReInviteCommand apiCommand = new ConferenceReInviteCommand(exten, phoneNUmber);
		SimpleResponse response = this.sendCommand(apiCommand, SimpleResponse.class);
		return response;
	}

	/**
	 * 会议室成员踢出
	 * 
	 * @return
	 */
	public SimpleResponse conferenceKick(String exten, String channel) {
		ConferenceKickCommand apiCommand = new ConferenceKickCommand(exten, channel);
		SimpleResponse response = this.sendCommand(apiCommand, SimpleResponse.class);
		return response;
	}

	/**
	 * 会议室成员静音
	 * 
	 * @return
	 */
	public SimpleResponse conferenceMute(String exten, String channel) {
		ConferenceMuteCommand apiCommand = new ConferenceMuteCommand(exten, channel);
		SimpleResponse response = this.sendCommand(apiCommand, SimpleResponse.class);
		return response;
	}

	/**
	 * 会议室成员解除静音
	 * 
	 * @return
	 */
	public SimpleResponse conferenceUnMute(String exten, String channel) {
		ConferenceUnmuteCommand apiCommand = new ConferenceUnmuteCommand(exten, channel);
		SimpleResponse response = this.sendCommand(apiCommand, SimpleResponse.class);
		return response;
	}
	
	/**
	 * 会议室锁定
	 * 
	 * @return
	 */
	public SimpleResponse conferenceLock(String exten) {
		ConferenceLockCommand apiCommand = new ConferenceLockCommand(exten);
		SimpleResponse response = this.sendCommand(apiCommand, SimpleResponse.class);
		return response;
	}
	
	/**
	 * 会议室解锁
	 * 
	 * @return
	 */
	public SimpleResponse conferenceUnlock(String exten) {
		ConferenceUnlockCommand apiCommand = new ConferenceUnlockCommand(exten);
		SimpleResponse response = this.sendCommand(apiCommand, SimpleResponse.class);
		return response;
	}

	///////////// http https ////////////////
	private static TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
		@Override
		public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
		}

		@Override
		public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
		}

		@Override
		public X509Certificate[] getAcceptedIssuers() {
			return null;
		}
	} };

	private class NullHostNameVerifier implements HostnameVerifier {
		@Override
		public boolean verify(String arg0, SSLSession arg1) {
			return true;
		}
	}

}
