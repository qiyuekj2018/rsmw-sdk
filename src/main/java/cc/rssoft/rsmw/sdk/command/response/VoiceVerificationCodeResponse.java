package cc.rssoft.rsmw.sdk.command.response;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class VoiceVerificationCodeResponse extends AbstractApiResponse{
	
	private String callUuid = "";

	public String getCallUuid() {
		return callUuid;
	}

	public void setCallUuid(String callUuid) {
		this.callUuid = callUuid;
	}
	
	
}
