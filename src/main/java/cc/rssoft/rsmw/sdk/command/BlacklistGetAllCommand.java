package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class BlacklistGetAllCommand extends AbstractApiCommand {

	private String command = "blacklistGetAll";

	public BlacklistGetAllCommand() {
		
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

}
