package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.bean.AccessNumber;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class AccessNumberUpdateCommand extends AbstractApiCommand {

	private String command = "accessNumberUpdate";

	private AccessNumber accessNumber = null;

	public AccessNumberUpdateCommand(AccessNumber accessNumber) {
	    this.setAccessNumber(accessNumber);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}



	public AccessNumber getAccessNumber() {
		return accessNumber;
	}

	public void setAccessNumber(AccessNumber accessNumber) {
		this.accessNumber = accessNumber;
	}



}
