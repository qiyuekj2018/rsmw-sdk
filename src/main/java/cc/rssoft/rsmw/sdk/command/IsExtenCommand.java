package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class IsExtenCommand extends AbstractApiCommand {

	private String command = "isExten";

	private String exten = "";

	public IsExtenCommand(String exten){
		setExten(exten);
	}
	
	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getExten() {
		return exten;
	}

	public void setExten(String exten) {
		this.exten = exten;
	}

	

}
