package cc.rssoft.rsmw.sdk.command.bean;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class QueueStatus {

	private Queue queue;
	
	private QueueParamsEvent queueParams;
	
	private QueueSummaryEvent queueSummary = new QueueSummaryEvent();
	
	private List<QueueEntryEvent> queueEntryList = new CopyOnWriteArrayList<>();
	
	/**
	 * key=SIP/8001, 
	 */
	private Map<String, QueueMemberBean> queueMemberEventMap = new ConcurrentHashMap<>();
	
	private Boolean needClean = false;

	public Queue getQueue() {
		return queue;
	}

	public void setQueue(Queue queue) {
		this.queue = queue;
	}

	public QueueParamsEvent getQueueParams() {
		return queueParams;
	}

	public void setQueueParams(QueueParamsEvent queueParams) {
		this.queueParams = queueParams;
	}

	public List<QueueEntryEvent> getQueueEntryList() {
		return queueEntryList;
	}

	public void setQueueEntryList(List<QueueEntryEvent> queueEntryList) {
		this.queueEntryList = queueEntryList;
	}

	public Map<String, QueueMemberBean> getQueueMemberEventMap() {
		return queueMemberEventMap;
	}

	public void setQueueMemberEventMap(Map<String, QueueMemberBean> queueMemberEventMap) {
		this.queueMemberEventMap = queueMemberEventMap;
	}

	public QueueSummaryEvent getQueueSummary() {
		return queueSummary;
	}

	public void setQueueSummary(QueueSummaryEvent queueSummary) {
		this.queueSummary = queueSummary;
	}

	public Boolean getNeedClean() {
		return needClean;
	}

	public void setNeedClean(Boolean needClean) {
		this.needClean = needClean;
	}
	
}
