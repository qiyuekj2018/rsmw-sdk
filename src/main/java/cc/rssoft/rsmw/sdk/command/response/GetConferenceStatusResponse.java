package cc.rssoft.rsmw.sdk.command.response;

import cc.rssoft.rsmw.sdk.command.bean.ConferenceStatus;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class GetConferenceStatusResponse extends AbstractApiResponse {

	private ConferenceStatus result = null;

	public ConferenceStatus getResult() {
		return result;
	}

	public void setResult(ConferenceStatus result) {
		this.result = result;
	}

}
