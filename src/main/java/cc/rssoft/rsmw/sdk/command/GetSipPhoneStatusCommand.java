package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class GetSipPhoneStatusCommand extends AbstractApiCommand {
	
	private String command = "getSipPhoneStatus";

	public GetSipPhoneStatusCommand(){
		
	}
	
	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

}
