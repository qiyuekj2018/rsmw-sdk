package cc.rssoft.rsmw.sdk.command.response;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class GetChannelCountResponse extends AbstractApiResponse {
	private Integer channels = 0;
	private Integer calls = 0;

	/**
	 * 实时通道数
	 * @return channels
	 */
	public Integer getChannels() {
		return channels;
	}

	public void setChannels(Integer channels) {
		this.channels = channels;
	}

	/**
	 * 实时通话数
	 * @return calls
	 */
	public Integer getCalls() {
		return calls;
	}

	public void setCalls(Integer calls) {
		this.calls = calls;
	}

}
