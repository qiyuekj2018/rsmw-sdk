package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class ConferenceMuteCommand extends AbstractApiCommand {

	private String command = "conferenceMute";

	private String exten = null;

	private String channel = null;

	public ConferenceMuteCommand(String exten, String channel) {
		this.exten = exten;
		this.channel = channel;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getExten() {
		return exten;
	}

	public void setExten(String exten) {
		this.exten = exten;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

}
