package cc.rssoft.rsmw.sdk.command.response;

import java.util.List;

import cc.rssoft.rsmw.sdk.command.bean.Queue;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class QueueGetAllResponse extends AbstractApiResponse {

	private List<Queue> result = null;

	public List<Queue> getResult() {
		return result;
	}

	public void setResult(List<Queue> result) {
		this.result = result;
	}

}
