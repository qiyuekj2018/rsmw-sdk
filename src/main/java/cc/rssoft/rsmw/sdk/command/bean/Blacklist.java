package cc.rssoft.rsmw.sdk.command.bean;

public class Blacklist {

    private Long id = null;
    private String phonenumber = null;
    private String typee = null;
    private String description = null;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getPhonenumber() {
        return phonenumber;
    }
    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }
    public String getTypee() {
        return typee;
    }
    public void setTypee(String typee) {
        this.typee = typee;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    
}
