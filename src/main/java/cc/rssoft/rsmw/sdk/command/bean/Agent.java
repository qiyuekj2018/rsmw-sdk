package cc.rssoft.rsmw.sdk.command.bean;

public class Agent {
	
	private Long id = null;
	
	/**
	 * 工号，用于Queue中的播报
	 */
	private Long  jobNumber= null;
	
	/**
	 * 在第三方系统的agent id，非rsmw_agent表的主键id
	 */
	private String aid = null;

	/**
	 * 姓名
	 */
	private String agentName = null;
	
	private String pwd = null;
	
	/**
	 * agent的姓名
	 * @return agentName
	 */
	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * agent的工号
	 * @return jobNumber
	 */
	public Long getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(Long jobNumber) {
		this.jobNumber = jobNumber;
	}

	/**
	 * agent的aid
	 * @return aid
	 */
	public String getAid() {
		return aid;
	}

	public void setAid(String aid) {
		this.aid = aid;
	}

	/**
	 * agent的id
	 * @return id
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

}
