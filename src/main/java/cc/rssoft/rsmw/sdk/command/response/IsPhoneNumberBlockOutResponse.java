package cc.rssoft.rsmw.sdk.command.response;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class IsPhoneNumberBlockOutResponse extends AbstractApiResponse {

	private Boolean isBlockOut = null;

	public Boolean getIsBlockOut() {
		return isBlockOut;
	}

	public void setIsBlockOut(Boolean isBlockOut) {
		this.isBlockOut = isBlockOut;
	}

}
