package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class AgentGetAllCommand extends AbstractApiCommand {

	private String command = "agentGetAll";

	public AgentGetAllCommand() {
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

}
