package cc.rssoft.rsmw.sdk.command.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SipPhoneStatus {

	private SipPhone sipPhone;

	// 此字段表达分机的注册状态。通过sippeeraction获取PeerEntryEvent来更新这两个字段
	private Boolean online = false;
	private String regStatus = "";
	private String ipAddress = "";
	private Integer port = 0;
	
	private Date busyStartTimestamp = null;//主要用来在界面显示通话时长
	//在new state event时设置对方号码
	private String connectedNumber = "";//主要用来在界面显示对方号码
	// CoreShowChannelEvent,Ring时设置，其他状态时清除
	private String ringingChannel = "";
	//在bridgeEvent时设置
	private String connectedChannel = "";
	//在newchannelevent时设置
	private String currentChannel = "";
	
	private String channelState = "";
	
	private Boolean isHold = false;

	//在绑定、解绑时更新此字段.
	//值为null，或具体的agent对象
	private Agent agent = null;//主要用来在界面显示绑定的座席

	//话机的置忙状态
	private String pauseStatus = "UNPAUSE";
	private String pauseReason = "";
	
	//该sipphone是哪些queue的member
	private List<QueueStatus> queueStatusList = new ArrayList<>();
	
	public Boolean getOnline() {
		return online;
	}

	public void setOnline(Boolean online) {
		this.online = online;
	}

	public Date getBusyStartTimestamp() {
		return busyStartTimestamp;
	}

	public void setBusyStartTimestamp(Date busyStartTimestamp) {
		this.busyStartTimestamp = busyStartTimestamp;
	}

	public String getConnectedNumber() {
		return connectedNumber;
	}

	public void setConnectedNumber(String connectedNumber) {
		this.connectedNumber = connectedNumber;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public String getRegStatus() {
		return regStatus;
	}

	public void setRegStatus(String regStatus) {
		this.regStatus = regStatus;
	}

	public String getCurrentChannel() {
		return currentChannel;
	}

	public void setCurrentChannel(String currentChannel) {
		this.currentChannel = currentChannel;
	}

	public SipPhone getSipPhone() {
		return sipPhone;
	}

	public void setSipPhone(SipPhone sipPhone) {
		this.sipPhone = sipPhone;
	}

	public String getConnectedChannel() {
		return connectedChannel;
	}

	public void setConnectedChannel(String connectedChannel) {
		this.connectedChannel = connectedChannel;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public List<QueueStatus> getQueueStatusList() {
		return queueStatusList;
	}

	public void setQueueStatusList(List<QueueStatus> queueStatusList) {
		this.queueStatusList = queueStatusList;
	}

	public String getPauseReason() {
		return pauseReason;
	}

	public void setPauseReason(String pauseReason) {
		this.pauseReason = pauseReason;
	}

	public String getPauseStatus() {
		return pauseStatus;
	}

	public void setPauseStatus(String pauseStatus) {
		this.pauseStatus = pauseStatus;
	}

	public Boolean getIsHold() {
		return isHold;
	}

	public void setIsHold(Boolean isHold) {
		this.isHold = isHold;
	}

	public String getRingingChannel() {
		return ringingChannel;
	}

	public void setRingingChannel(String ringingChannel) {
		this.ringingChannel = ringingChannel;
	}

	public String getChannelState() {
		return channelState;
	}

	public void setChannelState(String channelState) {
		this.channelState = channelState;
	}
}
