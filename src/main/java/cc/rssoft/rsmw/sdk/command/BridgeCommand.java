package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class BridgeCommand extends AbstractApiCommand {

	private String command = "bridge";

	private String channel1 = "";
	
	private String channel2 = "";

	public BridgeCommand(String channel1,String channel2) {
		setChannel1(channel1);
		setChannel2(channel2);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getChannel1() {
		return channel1;
	}

	public void setChannel1(String channel1) {
		this.channel1 = channel1;
	}

	public String getChannel2() {
		return channel2;
	}

	public void setChannel2(String channel2) {
		this.channel2 = channel2;
	}

}
