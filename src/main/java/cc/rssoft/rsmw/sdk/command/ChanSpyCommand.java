package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class ChanSpyCommand extends AbstractApiCommand {

	private String command = "chanSpy";

	private String spySipPhoneName = "";

	private String spiedSipPhoneName = "";

	public ChanSpyCommand(String spySipPhoneName, String spiedSipPhoneName) {
		setSpySipPhoneName(spySipPhoneName);
		setSpiedSipPhoneName(spiedSipPhoneName);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getSpySipPhoneName() {
		return spySipPhoneName;
	}

	public void setSpySipPhoneName(String spySipPhoneName) {
		this.spySipPhoneName = spySipPhoneName;
	}

	public String getSpiedSipPhoneName() {
		return spiedSipPhoneName;
	}

	public void setSpiedSipPhoneName(String spiedSipPhoneName) {
		this.spiedSipPhoneName = spiedSipPhoneName;
	}

}
