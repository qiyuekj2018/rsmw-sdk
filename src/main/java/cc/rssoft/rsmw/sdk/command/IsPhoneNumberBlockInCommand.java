package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class IsPhoneNumberBlockInCommand extends AbstractApiCommand {

	private String command = "isPhoneNumberBlockIn";

	private String phoneNumber = null;

	/**
	 * @param agentAid
	 * @param agentName
	 * @param agentJobNumber
	 */
	public IsPhoneNumberBlockInCommand(String phoneNumber) {
	    this.setPhoneNumber(phoneNumber);

	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}
