package cc.rssoft.rsmw.sdk.command.response;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class IsPhoneNumberBlockInResponse extends AbstractApiResponse {

	private Boolean isBlockIn = null;

	public Boolean getIsBlockIn() {
		return isBlockIn;
	}

	public void setIsBlockIn(Boolean isBlockIn) {
		this.isBlockIn = isBlockIn;
	}


}
