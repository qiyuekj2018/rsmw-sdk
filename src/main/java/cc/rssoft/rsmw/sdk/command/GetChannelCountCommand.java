package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class GetChannelCountCommand extends AbstractApiCommand {

	private String command = "getChannelCount";

	public GetChannelCountCommand() {

	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

}
