
package cc.rssoft.rsmw.sdk.command.internal;

import com.alibaba.fastjson.JSON;

public abstract class AbstractApiResponse {
	
	private long timestamp = 0L;
	
	private String errCode = "0";
	private String errMsg = "SUCCESS";
	
	public AbstractApiResponse(){
		setTimestamp(System.currentTimeMillis());
	}
	
	/**
	 * 将当前对象转换成JSON格式的字符串
	 * @return jsonString
	 */
	public String toJsonString(){
		return JSON.toJSONString(this);
	}
	
	/**
	 * 自1970年1月1日0时起的“毫秒数”<br>
	 * 调用者服务器的时间和RSMW服务器的时间误差不能超过10秒。否则系统会认为时间戳失效。
	 * @return timestamp
	 */
	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * 错误码，“0”代表成功，非0代表失败
	 * @return errCode
	 */
	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	/**
	 * 错误信息提示。“SUCCESS”代表成功，非SUCCESS代表失败
	 * @return errMsg
	 */
	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	} 

}
