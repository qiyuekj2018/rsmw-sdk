package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.bean.Queue;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class QueueSaveCommand extends AbstractApiCommand {

	private String command = "queueSave";

	private Queue queue = null;

	public QueueSaveCommand(Queue queue) {
	    this.setQueue(queue);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public Queue getQueue() {
		return queue;
	}

	public void setQueue(Queue queue) {
		this.queue = queue;
	}



}
