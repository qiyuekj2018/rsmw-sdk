package cc.rssoft.rsmw.sdk.command.bean;

public class Queue {

    private Long id = null;

    /**
     * 队列名称【必填】
     */
    private String name = "";

    /**
     * 队列分机号【必填】
     */
    private String exten = "";

    /**
     * 欢迎词，下拉，媒体文件 在进入Queue前可以先播放一段声音 自定义，【非必填】
     */
    private Long musicOnWelcomeQAnnounceId = null;

    /**
     * 背景音乐，下拉，moh【非必填】 Musicclass sets which music applies for this particular
     * call queue. ; The only class which can override this one is if the MOH
     * class is set ; directly on the channel using
     * Set(CHANNEL(musicclass)=whatever) in the ; dialplan.
     */
    private Long musicClassMohId = null;

    /**
     * 座席提示音，下拉，媒体文件【非必填】 ; An announcement may be specified which is played for
     * the member as ; soon as they answer a call, typically to indicate to them
     * which queue ; this call should be answered as, so that agents or members
     * who are ; listening to more than one queue can differentiated how they
     * should ; engage the customer
     */
    private Long agentAnnounceQAnnounceId = null;

    /**
     * 最大排队数【必填】
     */
    private Long maxLength = 0L;

    /**
     * 置忙数上限【必填】
     */
    private Long maxPauseCount = 10L;

    /**
     * 服务等级,input ，>=0数字 s,15【必填】 Second settings for service level (default 0)
     * Used for service level statistics (calls answered within service level
     * time frame)
     */
    private Long serviceLevel = 15L;

    /**
     * 振铃策略(ACD)，详见QueueStrategy枚举类【必填】
     */
    private String strategy = "";

    /**
     * 座席超时 input ，>=0数字 s【必填】
     */
    private Long agentTimeout = 5L;
    /**
     * 冷却时长【必填】
     */
    private Long wrapuptime = 0L;

    /**
     * 自动置忙【必填】
     */
    private String autoPause = "no";

    /**
     * 每天夜晚自动清理成员【必填】
     */
    private Boolean autoCleanMember = false;
    /**
     * 自动清理成员的时间
     */
    private String autoCleanMemberTime = "";

    /**
     * 接通座席前是否给客户播放座席的工号 报分机号 不报
     */
    private String announceJobNumber = "";
    private String language = "";
    private Long queueJobNumberQAnnounceId = null;// "工号"
    private Long queueAtYourServiceQAnnounceId = null;// “为您服务”

    /**
     * 开启服务质量评价 sqe = Service Quality Evaluation
     */
    private Boolean sqeEnable = false;
    /**
     * 服务质量评价语音rsmw_sqeQuestion
     */
    private Long sqeQuestionId = null;

    /**
     * 服务质量评价语音rsmw_sqeThankyou
     */
    private Long sqeThankyouId = null;

    /**
     * 启用announce【必填】
     */
    private Boolean enableAnnounce = false;

    /**
     * 提示间隔【必填】
     * 
     * How often to announce queue position and/or estimated holdtime to caller
     * (0=off). Note that this value is ignored if the caller's queue position
     * has changed (see min-announce-frequency)
     */
    private Long announceFrequency = 90L;

    /**
     * 提示间隔【必填】
     * 
     * The absolute minimum time between the start of each queue position and/or
     * estimated holdtime announcement. This is useful for avoiding constant
     * announcements when the caller's queue position is changing frequently
     * (see announce-frequency)
     */
    private Long minAnnounceFrequency = 15L;

    // 提示语音【必填】
    private Long queueYouAreNextQAnnounceId = null;// 您当前排在第一位【非必填】
    private Long queueThereAreQAnnounceId = null;// 目前有【非必填】
    private Long queueCallsWatingQAnnounceId = null;// 个呼叫正在排队【非必填】
    private Long queueHoldTimeQAnnounceId = null;// 等待时长【非必填】
    private Long queueMinuteQAnnounceId = null;// 分【非必填】
    private Long queueMinutesQAnnounceId = null;// 分【非必填】
    private Long queueSecondsQAnnounceId = null;// 秒【非必填】
    private Long queueThankyouQAnnounceId = null;// 感谢您的耐心等待【非必填】
    private Long queueReportholdQAnnounceId = null;// 播报等待时长【非必填】
    private Long queuePeriodicAnnounceQAnnounceId = null;// 目前座席繁忙，请耐心等待【非必填】

    /**
     * 技能组超时，input ，>=0数字 s【必填】
     */
    private Long queueTimeout = 1800L;

    /**
     * 目标路由的exten类型
     */
    private String destTypeAfterTimeout = "";

    /**
     * 超时后的目标路由的具体exten
     */
    private String destAfterTimeout = "";
    private String dstAccessNumber = "";
    private String dstGateway = "";

    // ---------------------高级选项-------------------------------------

    /**
     * 隐藏号码
     */
    private Boolean hideCallerId = false;

    // ---------------------高级选项-------------------------------------

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExten() {
        return exten;
    }

    public void setExten(String exten) {
        this.exten = exten;
    }

    public Long getMusicClassMohId() {
        return musicClassMohId;
    }

    public void setMusicClassMohId(Long musicClassMohId) {
        this.musicClassMohId = musicClassMohId;
    }

    public String getStrategy() {
        return strategy;
    }

    public void setStrategy(String strategy) {
        this.strategy = strategy;
    }

    public Long getQueueTimeout() {
        return queueTimeout;
    }

    public void setQueueTimeout(Long queueTimeout) {
        this.queueTimeout = queueTimeout;
    }

    public Long getAgentTimeout() {
        return agentTimeout;
    }

    public void setAgentTimeout(Long agentTimeout) {
        this.agentTimeout = agentTimeout;
    }

    public Long getServiceLevel() {
        return serviceLevel;
    }

    public void setServiceLevel(Long serviceLevel) {
        this.serviceLevel = serviceLevel;
    }

    public Long getWrapuptime() {
        return wrapuptime;
    }

    public void setWrapuptime(Long wrapuptime) {
        this.wrapuptime = wrapuptime;
    }

    public String getAutoPause() {
        return autoPause;
    }

    public void setAutoPause(String autoPause) {
        this.autoPause = autoPause;
    }

    public Long getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(Long maxLength) {
        this.maxLength = maxLength;
    }

    public Long getAnnounceFrequency() {
        return announceFrequency;
    }

    public void setAnnounceFrequency(Long announceFrequency) {
        this.announceFrequency = announceFrequency;
    }

    public Long getMinAnnounceFrequency() {
        return minAnnounceFrequency;
    }

    public void setMinAnnounceFrequency(Long minAnnounceFrequency) {
        this.minAnnounceFrequency = minAnnounceFrequency;
    }

    public Long getQueueYouAreNextQAnnounceId() {
        return queueYouAreNextQAnnounceId;
    }

    public void setQueueYouAreNextQAnnounceId(Long queueYouAreNextQAnnounceId) {
        this.queueYouAreNextQAnnounceId = queueYouAreNextQAnnounceId;
    }

    public Long getQueueThereAreQAnnounceId() {
        return queueThereAreQAnnounceId;
    }

    public void setQueueThereAreQAnnounceId(Long queueThereAreQAnnounceId) {
        this.queueThereAreQAnnounceId = queueThereAreQAnnounceId;
    }

    public Long getQueueCallsWatingQAnnounceId() {
        return queueCallsWatingQAnnounceId;
    }

    public void setQueueCallsWatingQAnnounceId(Long queueCallsWatingQAnnounceId) {
        this.queueCallsWatingQAnnounceId = queueCallsWatingQAnnounceId;
    }

    public Long getQueueHoldTimeQAnnounceId() {
        return queueHoldTimeQAnnounceId;
    }

    public void setQueueHoldTimeQAnnounceId(Long queueHoldTimeQAnnounceId) {
        this.queueHoldTimeQAnnounceId = queueHoldTimeQAnnounceId;
    }

    public Long getQueueMinuteQAnnounceId() {
        return queueMinuteQAnnounceId;
    }

    public void setQueueMinuteQAnnounceId(Long queueMinuteQAnnounceId) {
        this.queueMinuteQAnnounceId = queueMinuteQAnnounceId;
    }

    public Long getQueueMinutesQAnnounceId() {
        return queueMinutesQAnnounceId;
    }

    public void setQueueMinutesQAnnounceId(Long queueMinutesQAnnounceId) {
        this.queueMinutesQAnnounceId = queueMinutesQAnnounceId;
    }

    public Long getQueueSecondsQAnnounceId() {
        return queueSecondsQAnnounceId;
    }

    public void setQueueSecondsQAnnounceId(Long queueSecondsQAnnounceId) {
        this.queueSecondsQAnnounceId = queueSecondsQAnnounceId;
    }

    public Long getQueueThankyouQAnnounceId() {
        return queueThankyouQAnnounceId;
    }

    public void setQueueThankyouQAnnounceId(Long queueThankyouQAnnounceId) {
        this.queueThankyouQAnnounceId = queueThankyouQAnnounceId;
    }

    public Long getQueueReportholdQAnnounceId() {
        return queueReportholdQAnnounceId;
    }

    public void setQueueReportholdQAnnounceId(Long queueReportholdQAnnounceId) {
        this.queueReportholdQAnnounceId = queueReportholdQAnnounceId;
    }

    public Long getQueuePeriodicAnnounceQAnnounceId() {
        return queuePeriodicAnnounceQAnnounceId;
    }

    public void setQueuePeriodicAnnounceQAnnounceId(Long queuePeriodicAnnounceQAnnounceId) {
        this.queuePeriodicAnnounceQAnnounceId = queuePeriodicAnnounceQAnnounceId;
    }

    public Boolean getAutoCleanMember() {
        return autoCleanMember;
    }

    public void setAutoCleanMember(Boolean autoCleanMember) {
        this.autoCleanMember = autoCleanMember;
    }

    public Boolean getEnableAnnounce() {
        return enableAnnounce;
    }

    public void setEnableAnnounce(Boolean enableAnnounce) {
        this.enableAnnounce = enableAnnounce;
    }

    public String getAutoCleanMemberTime() {
        return autoCleanMemberTime;
    }

    public void setAutoCleanMemberTime(String autoCleanMemberTime) {
        this.autoCleanMemberTime = autoCleanMemberTime;
    }

    public String getAnnounceJobNumber() {
        return announceJobNumber;
    }

    public void setAnnounceJobNumber(String announceJobNumber) {
        this.announceJobNumber = announceJobNumber;
    }

    public Long getQueueJobNumberQAnnounceId() {
        return queueJobNumberQAnnounceId;
    }

    public void setQueueJobNumberQAnnounceId(Long queueJobNumberQAnnounceId) {
        this.queueJobNumberQAnnounceId = queueJobNumberQAnnounceId;
    }

    public Long getQueueAtYourServiceQAnnounceId() {
        return queueAtYourServiceQAnnounceId;
    }

    public void setQueueAtYourServiceQAnnounceId(Long queueAtYourServiceQAnnounceId) {
        this.queueAtYourServiceQAnnounceId = queueAtYourServiceQAnnounceId;
    }

    public Long getSqeQuestionId() {
        return sqeQuestionId;
    }

    public void setSqeQuestionId(Long sqeQuestionId) {
        this.sqeQuestionId = sqeQuestionId;
    }

    public Long getSqeThankyouId() {
        return sqeThankyouId;
    }

    public void setSqeThankyouId(Long sqeThankyouId) {
        this.sqeThankyouId = sqeThankyouId;
    }

    public Long getMusicOnWelcomeQAnnounceId() {
        return musicOnWelcomeQAnnounceId;
    }

    public void setMusicOnWelcomeQAnnounceId(Long musicOnWelcomeQAnnounceId) {
        this.musicOnWelcomeQAnnounceId = musicOnWelcomeQAnnounceId;
    }

    public Long getAgentAnnounceQAnnounceId() {
        return agentAnnounceQAnnounceId;
    }

    public void setAgentAnnounceQAnnounceId(Long agentAnnounceQAnnounceId) {
        this.agentAnnounceQAnnounceId = agentAnnounceQAnnounceId;
    }

    public String getDestTypeAfterTimeout() {
        return destTypeAfterTimeout;
    }

    public void setDestTypeAfterTimeout(String destTypeAfterTimeout) {
        this.destTypeAfterTimeout = destTypeAfterTimeout;
    }

    public String getDestAfterTimeout() {
        return destAfterTimeout;
    }

    public void setDestAfterTimeout(String destAfterTimeout) {
        this.destAfterTimeout = destAfterTimeout;
    }

    public String getDstAccessNumber() {
        return dstAccessNumber;
    }

    public void setDstAccessNumber(String dstAccessNumber) {
        this.dstAccessNumber = dstAccessNumber;
    }

    public String getDstGateway() {
        return dstGateway;
    }

    public void setDstGateway(String dstGateway) {
        this.dstGateway = dstGateway;
    }

    public Boolean getSqeEnable() {
        return sqeEnable;
    }

    public void setSqeEnable(Boolean sqeEnable) {
        this.sqeEnable = sqeEnable;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Long getMaxPauseCount() {
        return maxPauseCount;
    }

    public void setMaxPauseCount(Long maxPauseCount) {
        this.maxPauseCount = maxPauseCount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getHideCallerId() {
        return hideCallerId;
    }

    public void setHideCallerId(Boolean hideCallerId) {
        this.hideCallerId = hideCallerId;
    }
}
