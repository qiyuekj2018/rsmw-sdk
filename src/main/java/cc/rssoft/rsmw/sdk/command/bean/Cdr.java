package cc.rssoft.rsmw.sdk.command.bean;

import java.util.Date;

/**
 * 
 * cdr
 * 
 * @author jiang
 *
 */
public class Cdr  {

	private String serverId = "";
	private String callUuid = "";
	private String uniqueid = "";
	private String callerId = "";
	private String src = "";
	private String dstContext = "";
	private String dst = "";
	private String srcChannel = "";
	private String dstChannel = "";
	private Date startTime = new Date(0);
	private Date answerTime = new Date(0);
	private Date endTime = new Date(0);
	private Long duration = 0L;
	private Long billSeconds = 0L;
	private String disposition = "";
	private String lastApplication = "";
	private String lastData = "";
	private String accountcode = "";
	private String userfield = "";
	
	private String srcAgentAid = "";
	private String srcAgentName = "";
	private String dstAgentAid = "";
	private String dstAgentName = "";
	
	private String srcAccessNumber = "";
	private String srcGateway = "";
	private String dstAccessNumber = "";
	private String dstGateway = "";

	private String callDirection = "";
	private Boolean answered = false;
	
	private String recordUrl = "";
	
	private Integer score = null;

	private String scoreComment = "";
	
	
	public String getSrcAgentAid() {
		return srcAgentAid;
	}
	public void setSrcAgentAid(String srcAgentAid) {
		this.srcAgentAid = srcAgentAid;
	}
	public String getDstAgentAid() {
		return dstAgentAid;
	}
	public void setDstAgentAid(String dstAgentAid) {
		this.dstAgentAid = dstAgentAid;
	}
	public String getServerId() {
		return serverId;
	}
	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	public String getUniqueid() {
		return uniqueid;
	}
	public void setUniqueid(String uniqueid) {
		this.uniqueid = uniqueid;
	}
	public String getCallerId() {
		return callerId;
	}
	public void setCallerId(String callerId) {
		this.callerId = callerId;
	}
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}
	public String getDstContext() {
		return dstContext;
	}
	public void setDstContext(String dstContext) {
		this.dstContext = dstContext;
	}
	public String getDst() {
		return dst;
	}
	public void setDst(String dst) {
		this.dst = dst;
	}
	public String getSrcChannel() {
		return srcChannel;
	}
	public void setSrcChannel(String srcChannel) {
		this.srcChannel = srcChannel;
	}
	public String getDstChannel() {
		return dstChannel;
	}
	public void setDstChannel(String dstChannel) {
		this.dstChannel = dstChannel;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getAnswerTime() {
		return answerTime;
	}
	public void setAnswerTime(Date answerTime) {
		this.answerTime = answerTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Long getDuration() {
		return duration;
	}
	public void setDuration(Long duration) {
		this.duration = duration;
	}
	public Long getBillSeconds() {
		return billSeconds;
	}
	public void setBillSeconds(Long billSeconds) {
		this.billSeconds = billSeconds;
	}
	public String getDisposition() {
		return disposition;
	}
	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}
	public String getLastApplication() {
		return lastApplication;
	}
	public void setLastApplication(String lastApplication) {
		this.lastApplication = lastApplication;
	}
	public String getLastData() {
		return lastData;
	}
	public void setLastData(String lastData) {
		this.lastData = lastData;
	}
	public String getAccountcode() {
		return accountcode;
	}
	public void setAccountcode(String accountcode) {
		this.accountcode = accountcode;
	}
	public String getUserfield() {
		return userfield;
	}
	public void setUserfield(String userfield) {
		this.userfield = userfield;
	}
	public String getSrcAccessNumber() {
		return srcAccessNumber;
	}
	public void setSrcAccessNumber(String srcAccessNumber) {
		this.srcAccessNumber = srcAccessNumber;
	}
	public String getDstAccessNumber() {
		return dstAccessNumber;
	}
	public void setDstAccessNumber(String dstAccessNumber) {
		this.dstAccessNumber = dstAccessNumber;
	}
	public String getCallDirection() {
		return callDirection;
	}
	public void setCallDirection(String callDirection) {
		this.callDirection = callDirection;
	}
	public String getRecordUrl() {
		return recordUrl;
	}
	public void setRecordUrl(String recordUrl) {
		this.recordUrl = recordUrl;
	}
	public String getSrcGateway() {
		return srcGateway;
	}
	public void setSrcGateway(String srcGateway) {
		this.srcGateway = srcGateway;
	}
	public String getDstGateway() {
		return dstGateway;
	}
	public void setDstGateway(String dstGateway) {
		this.dstGateway = dstGateway;
	}
	public Boolean getAnswered() {
		return answered;
	}
	public void setAnswered(Boolean answered) {
		this.answered = answered;
	}
	public String getSrcAgentName() {
		return srcAgentName;
	}
	public void setSrcAgentName(String srcAgentName) {
		this.srcAgentName = srcAgentName;
	}
	public String getDstAgentName() {
		return dstAgentName;
	}
	public void setDstAgentName(String dstAgentName) {
		this.dstAgentName = dstAgentName;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public String getScoreComment() {
		return scoreComment;
	}
	public void setScoreComment(String scoreComment) {
		this.scoreComment = scoreComment;
	}
	public String getCallUuid() {
		return callUuid;
	}
	public void setCallUuid(String callUuid) {
		this.callUuid = callUuid;
	}

}
