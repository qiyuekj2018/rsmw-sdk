package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class GetChannelVariablesCommand extends AbstractApiCommand {
	
	private String command = "getChannelVariables";

	public GetChannelVariablesCommand(){
		
	}
	
	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

}
