package cc.rssoft.rsmw.sdk.command.response;

import java.util.Map;

import cc.rssoft.rsmw.sdk.command.bean.QueueStatus;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class GetQueueStatusResponse extends AbstractApiResponse {

	private Map<String, QueueStatus> result = null;

	/**
	 * 所有的技能组<br>
	 * key=exten, value=queueStatus对象
	 */
	public Map<String, QueueStatus> getResult() {
		return result;
	}

	public void setResult(Map<String, QueueStatus> result) {
		this.result = result;
	}


}
