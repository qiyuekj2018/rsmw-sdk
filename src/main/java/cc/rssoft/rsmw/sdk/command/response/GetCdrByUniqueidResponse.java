package cc.rssoft.rsmw.sdk.command.response;

import cc.rssoft.rsmw.sdk.command.bean.Cdr;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class GetCdrByUniqueidResponse extends AbstractApiResponse {

	private Cdr cdr;

	public Cdr getCdr() {
		return cdr;
	}

	public void setCdr(Cdr cdr) {
		this.cdr = cdr;
	}
	
	
}
