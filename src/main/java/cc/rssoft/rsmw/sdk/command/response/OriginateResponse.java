package cc.rssoft.rsmw.sdk.command.response;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class OriginateResponse extends AbstractApiResponse{
	
	private String callUuid = "";
	private Integer queueLength = -1;

	/**
	 * RSMW 内部呼叫队列中，当前排队的呼叫请求数量
	 * @return queueLength
	 */
	public Integer getQueueLength() {
		return queueLength;
	}

	public void setQueueLength(Integer queueLength) {
		this.queueLength = queueLength;
	}

	public String getCallUuid() {
		return callUuid;
	}

	public void setCallUuid(String callUuid) {
		this.callUuid = callUuid;
	}
	
	
}
