package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class SipPhoneGetAllCommand extends AbstractApiCommand {

	private String command = "sipPhoneGetAll";

	public SipPhoneGetAllCommand() {
		
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

}
