package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class HangupCommand extends AbstractApiCommand {
	
	private String command = "hangup";
	
	private String channel = "";

	/**
	 * @param channel
	 */
	public HangupCommand(String channel) {
		this.channel = channel;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

}
