package cc.rssoft.rsmw.sdk.command;

import java.util.List;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class ConferenceInviteCommand extends AbstractApiCommand {

	private String command = "conferenceInvite";

	private String exten = null;

	private String gateway = null;
	
	private String accessNumber = null;
	
	private List<String> phoneNumberList = null;

	public ConferenceInviteCommand(String exten,String gateway, String accessNumber,List<String> phoneNumberList) {
		this.exten = exten;
		this.gateway = gateway;
		
		this.accessNumber = accessNumber;
		
		this.phoneNumberList = phoneNumberList;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getExten() {
		return exten;
	}

	public void setExten(String exten) {
		this.exten = exten;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getAccessNumber() {
		return accessNumber;
	}

	public void setAccessNumber(String accessNumber) {
		this.accessNumber = accessNumber;
	}

	public List<String> getPhoneNumberList() {
		return phoneNumberList;
	}

	public void setPhoneNumberList(List<String> phoneNumberList) {
		this.phoneNumberList = phoneNumberList;
	}

}
