package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class RedirectCommand extends AbstractApiCommand {

	private String command = "redirect";
	private String channel = "";
	private String context = "";
	private String exten = "";
	private String dstGateway = "";
	private String dstAccessNumber = "";
	private String extraDstGateway = "";
	private String extraDdstAccessNumber = "";	
	private String extraChannel = "";
	private String extraExten = "";
	private String extraContext = "";

	/**
	 * @param channel
	 * @param exten
	 */
	public RedirectCommand(String channel, String context, String exten, String dstGateway, String dstAccessNumber, String extraChannel, String extraContext, String extraExten, String extraDstGateway, String extraDstAccessNumber) {
		this.channel = channel;
		this.context = context;
		this.exten = exten;
		this.dstGateway = dstGateway;
		this.dstAccessNumber = dstAccessNumber;
		
		this.extraChannel = extraChannel;
		this.extraContext = extraContext;
		this.extraExten = extraExten;
		this.extraDstGateway = extraDstGateway;
		this.extraDdstAccessNumber = extraDstAccessNumber;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getExten() {
		return exten;
	}

	public void setExten(String exten) {
		this.exten = exten;
	}

	public String getExtraChannel() {
		return extraChannel;
	}

	public void setExtraChannel(String extraChannel) {
		this.extraChannel = extraChannel;
	}

	public String getExtraExten() {
		return extraExten;
	}

	public void setExtraExten(String extraExten) {
		this.extraExten = extraExten;
	}

	public String getDstGateway() {
		return dstGateway;
	}

	public void setDstGateway(String dstGateway) {
		this.dstGateway = dstGateway;
	}

	public String getDstAccessNumber() {
		return dstAccessNumber;
	}

	public void setDstAccessNumber(String dstAccessNumber) {
		this.dstAccessNumber = dstAccessNumber;
	}

	public String getExtraDstGateway() {
		return extraDstGateway;
	}

	public void setExtraDstGateway(String extraDstGateway) {
		this.extraDstGateway = extraDstGateway;
	}

	public String getExtraDdstAccessNumber() {
		return extraDdstAccessNumber;
	}

	public void setExtraDdstAccessNumber(String extraDdstAccessNumber) {
		this.extraDdstAccessNumber = extraDdstAccessNumber;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getExtraContext() {
		return extraContext;
	}

	public void setExtraContext(String extraContext) {
		this.extraContext = extraContext;
	}

}
