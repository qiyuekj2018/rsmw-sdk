package cc.rssoft.rsmw.sdk.command.response;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class GetChannelResponse extends AbstractApiResponse {

	private String channel = "";

	private String connectedChannel = "";

	/**
	 * sipPhone的channel
	 * @return channel
	 */
	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	/**
	 * 正与sipPhone接通的对方channel
	 * @return connectedChannel
	 */
	public String getConnectedChannel() {
		return connectedChannel;
	}

	public void setConnectedChannel(String connectedChannel) {
		this.connectedChannel = connectedChannel;
	}

}
