package cc.rssoft.rsmw.sdk.command.response;

import java.util.List;

import cc.rssoft.rsmw.sdk.command.bean.SipPhone;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class SipPhoneGetAllResponse extends AbstractApiResponse {

	private List<SipPhone> result = null;

	public List<SipPhone> getResult() {
		return result;
	}

	public void setResult(List<SipPhone> result) {
		this.result = result;
	}

}
