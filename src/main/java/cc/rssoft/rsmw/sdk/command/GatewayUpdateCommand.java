package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.bean.Gateway;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class GatewayUpdateCommand extends AbstractApiCommand {

	private String command = "gatewayUpdate";

	private Gateway gateway = null;

	public GatewayUpdateCommand(Gateway gateway) {
	    this.setGateway(gateway);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public Gateway getGateway() {
		return gateway;
	}

	public void setGateway(Gateway gateway) {
		this.gateway = gateway;
	}



}
