package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class BindCommand extends AbstractApiCommand {

	private String command = "bind";
	
	private String agentAid = "";
	
	private String sipPhoneName = "";

	/**
	 * @param agentAid
	 * @param sipPhoneName
	 */
	public BindCommand(String agentAid, String sipPhoneName) {
		this.agentAid = agentAid;
		this.sipPhoneName = sipPhoneName;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getAgentAid() {
		return agentAid;
	}

	public void setAgentAid(String agentAid) {
		this.agentAid = agentAid;
	}

	public String getSipPhoneName() {
		return sipPhoneName;
	}

	public void setSipPhoneName(String sipPhoneName) {
		this.sipPhoneName = sipPhoneName;
	}
	

}
