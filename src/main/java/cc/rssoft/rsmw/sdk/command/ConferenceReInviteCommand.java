package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class ConferenceReInviteCommand extends AbstractApiCommand {

	private String command = "conferenceReInvite";
	
	private String exten = null;

	private String phoneNumber = null;

	public ConferenceReInviteCommand(String exten, String phoneNumber) {
		this.exten = exten;
		this.phoneNumber = phoneNumber;

	}

	public String getExten() {
		return exten;
	}

	public void setExten(String exten) {
		this.exten = exten;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

}
