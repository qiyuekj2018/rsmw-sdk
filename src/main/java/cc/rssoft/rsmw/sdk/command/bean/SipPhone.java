package cc.rssoft.rsmw.sdk.command.bean;

public class SipPhone {
	private Long id = null;
	private String name = "";
	private String secret = "";
	private Integer timeout = null;
	private String callerIdNumber = "";
	private String dstGateway = "";//非必填
	private Boolean canNotCallWithoutBind = null;
	private Boolean canLdd = null;
	private Boolean canDdd = null;
	private Boolean canIdd = null;
	private Boolean hideCallerId = null;
	
	/**
	 * 编码1
	 */
	private String codec1 = "";
	/**
	 * 编码2
	 */
	private String codec2 = "";
	/**
	 * 编码3
	 */
	private String codec3 = "";
	/**
	 * 编码4
	 */
	private String codec4 = "";
	
	/**
	 * 呼叫转移类型
	 */
	private String  callForwardType = "";
	
	/**
	 * 呼叫转移号码
	 */
	private String callForwardNumber = "";
	
	/**
	 * 呼叫转移所用的gateway
	 */
	private String callForwardGateway = "";
	
	/**
	 * 呼叫转移所用的accessNumber
	 */	
	private String callForwardAccessNumber = "";
	
	/**
	 * sipPhone的id
	 * @return id
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * sipPhone的分机号
	 * @return name
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * sipPhone注册密码
	 * @return secret
	 */
	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	/**
	 * sipPhone主叫号
	 * @return callerIdNumber
	 */
	public String getCallerIdNumber() {
		return callerIdNumber;
	}

	public void setCallerIdNumber(String callerIdNumber) {
		this.callerIdNumber = callerIdNumber;
	}

	/**
	 * 允许呼叫本地号码
	 * @return canLdd
	 */
	public Boolean getCanLdd() {
		return canLdd;
	}

	public void setCanLdd(Boolean canLdd) {
		this.canLdd = canLdd;
	}

	/**
	 * 允许呼叫国内长途
	 * @return canDdd
	 */
	public Boolean getCanDdd() {
		return canDdd;
	}

	public void setCanDdd(Boolean canDdd) {
		this.canDdd = canDdd;
	}

	/**
	 * 允许呼叫国际长途
	 * @return canIdd
	 */
	public Boolean getCanIdd() {
		return canIdd;
	}

	public void setCanIdd(Boolean canIdd) {
		this.canIdd = canIdd;
	}

	public String getDstGateway() {
		return dstGateway;
	}

	public void setDstGateway(String dstGateway) {
		this.dstGateway = dstGateway;
	}

	public String getCallForwardType() {
		return callForwardType;
	}

	public void setCallForwardType(String callForwardType) {
		this.callForwardType = callForwardType;
	}

	public String getCallForwardNumber() {
		return callForwardNumber;
	}

	public void setCallForwardNumber(String callForwardNumber) {
		this.callForwardNumber = callForwardNumber;
	}

	public String getCallForwardGateway() {
		return callForwardGateway;
	}

	public void setCallForwardGateway(String callForwardGateway) {
		this.callForwardGateway = callForwardGateway;
	}

	public String getCallForwardAccessNumber() {
		return callForwardAccessNumber;
	}

	public void setCallForwardAccessNumber(String callForwardAccessNumber) {
		this.callForwardAccessNumber = callForwardAccessNumber;
	}

	public Boolean getCanNotCallWithoutBind() {
		return canNotCallWithoutBind;
	}

	public void setCanNotCallWithoutBind(Boolean canNotCallWithoutBind) {
		this.canNotCallWithoutBind = canNotCallWithoutBind;
	}

	public Boolean getHideCallerId() {
		return hideCallerId;
	}

	public void setHideCallerId(Boolean hideCallerId) {
		this.hideCallerId = hideCallerId;
	}

	public Integer getTimeout() {
		return timeout;
	}

	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}

	public String getCodec1() {
		return codec1;
	}

	public void setCodec1(String codec1) {
		this.codec1 = codec1;
	}

	public String getCodec2() {
		return codec2;
	}

	public void setCodec2(String codec2) {
		this.codec2 = codec2;
	}

	public String getCodec3() {
		return codec3;
	}

	public void setCodec3(String codec3) {
		this.codec3 = codec3;
	}

	public String getCodec4() {
		return codec4;
	}

	public void setCodec4(String codec4) {
		this.codec4 = codec4;
	}

}
