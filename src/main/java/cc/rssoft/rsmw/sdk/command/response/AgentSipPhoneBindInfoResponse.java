package cc.rssoft.rsmw.sdk.command.response;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class AgentSipPhoneBindInfoResponse extends AbstractApiResponse {

	private String agentAid = "";

	private String sipPhoneName = "";

	/**
	 * 第三方应用中用户的唯一标识，一般是id或用户名
	 * @return agentAid
	 */
	public String getAgentAid() {
		return agentAid;
	}

	public void setAgentAid(String agentAid) {
		this.agentAid = agentAid;
	}

	/**
	 * 所要绑定的SIPPHONE的分机号
	 * @return sipPhoneName
	 */
	public String getSipPhoneName() {
		return sipPhoneName;
	}

	public void setSipPhoneName(String sipPhoneName) {
		this.sipPhoneName = sipPhoneName;
	}

}
