package cc.rssoft.rsmw.sdk.command.response;

import java.util.List;

import cc.rssoft.rsmw.sdk.command.bean.Gateway;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class GatewayGetAllResponse extends AbstractApiResponse {

	private List<Gateway> result = null;

	public List<Gateway> getResult() {
		return result;
	}

	public void setResult(List<Gateway> result) {
		this.result = result;
	}


}
