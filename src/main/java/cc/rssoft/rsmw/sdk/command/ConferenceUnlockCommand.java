package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class ConferenceUnlockCommand extends AbstractApiCommand {

	private String command = "conferenceUnlock";

	private String exten = null;

	public ConferenceUnlockCommand(String exten) {
		this.exten = exten;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getExten() {
		return exten;
	}

	public void setExten(String exten) {
		this.exten = exten;
	}

}
