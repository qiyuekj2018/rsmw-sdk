package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class SqeCommand extends AbstractApiCommand {

	private String command = "sqe";

	private String sqeSipPhoneName = "";

	private String sqeQuestion = "";

	private String sqeThankyou = "";

	public SqeCommand(String sqeSipPhoneName, String sqeQuestion, String sqeThankyou) {
		setSqeSipPhoneName(sqeSipPhoneName);
		setSqeQuestion(sqeQuestion);
		setSqeThankyou(sqeThankyou);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getSqeSipPhoneName() {
		return sqeSipPhoneName;
	}

	public void setSqeSipPhoneName(String sqeSipPhoneName) {
		this.sqeSipPhoneName = sqeSipPhoneName;
	}

	public String getSqeQuestion() {
		return sqeQuestion;
	}

	public void setSqeQuestion(String sqeQuestion) {
		this.sqeQuestion = sqeQuestion;
	}

	public String getSqeThankyou() {
		return sqeThankyou;
	}

	public void setSqeThankyou(String sqeThankyou) {
		this.sqeThankyou = sqeThankyou;
	}

}
