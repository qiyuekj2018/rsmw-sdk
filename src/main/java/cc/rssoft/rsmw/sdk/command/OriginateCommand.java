package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class OriginateCommand extends AbstractApiCommand {

	private String command = "originate";

	private Integer absoluteTimeout = 0;
	private String src = "";
	private String srcGateway = "";
	private String srcAccessNumber = "";
	private String srcCallerIdName = "";
	private String srcCallerIdNumber = "";
	private String srcAnnounceMediaUrl = "";
	private String dst = "";
	private String dstGateway = "";
	private String dstAccessNumber = "";
	private String dstAnnounceMediaUrl = "";
	private String userData = "";
	
	
	/**
	 * @param src
	 * @param dst
	 */
	public OriginateCommand(String src, String dst){
		this.src = src;
		this.dst = dst;
	}
	
	/**
	 * @param src
	 * @param srcGateway
	 * @param srcAccessNumber
	 * @param srcAnnounceMediaUrl
	 * @param dst
	 * @param dstGateway
	 * @param dstAccessNumber
	 * @param dstAnnounceMediaUrl
	 */
	public OriginateCommand(Integer absoluteTimeout, String src, String srcGateway, String srcAccessNumber, String srcCallerIdName,String srcCallerIdNumber,String srcAnnounceMediaUrl, String dst, String dstGateway, String dstAccessNumber,String dstAnnounceMediaUrl, String userData){
		this.absoluteTimeout = absoluteTimeout;
		this.src = src;
		this.srcGateway = srcGateway;
		this.srcAccessNumber = srcAccessNumber;
		this.srcCallerIdName = srcCallerIdName;
		this.srcCallerIdNumber = srcCallerIdNumber;
		this.srcAnnounceMediaUrl = srcAnnounceMediaUrl;
		this.dst = dst;
		this.dstGateway = dstGateway;
		this.dstAccessNumber = dstAccessNumber;
		this.dstAnnounceMediaUrl = dstAnnounceMediaUrl;
		this.userData = userData;
	}
	
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public Integer getAbsoluteTimeout() {
		return absoluteTimeout;
	}
	public void setAbsoluteTimeout(Integer absoluteTimeout) {
		this.absoluteTimeout = absoluteTimeout;
	}
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}
	public String getSrcGateway() {
		return srcGateway;
	}
	public void setSrcGateway(String srcGateway) {
		this.srcGateway = srcGateway;
	}
	public String getSrcAccessNumber() {
		return srcAccessNumber;
	}
	public void setSrcAccessNumber(String srcAccessNumber) {
		this.srcAccessNumber = srcAccessNumber;
	}
	public String getSrcAnnounceMediaUrl() {
		return srcAnnounceMediaUrl;
	}
	public void setSrcAnnounceMediaUrl(String srcAnnounceMediaUrl) {
		this.srcAnnounceMediaUrl = srcAnnounceMediaUrl;
	}
	public String getDst() {
		return dst;
	}
	public void setDst(String dst) {
		this.dst = dst;
	}
	public String getDstGateway() {
		return dstGateway;
	}
	public void setDstGateway(String dstGateway) {
		this.dstGateway = dstGateway;
	}
	public String getDstAccessNumber() {
		return dstAccessNumber;
	}
	public void setDstAccessNumber(String dstAccessNumber) {
		this.dstAccessNumber = dstAccessNumber;
	}
	public String getDstAnnounceMediaUrl() {
		return dstAnnounceMediaUrl;
	}
	public void setDstAnnounceMediaUrl(String dstAnnounceMediaUrl) {
		this.dstAnnounceMediaUrl = dstAnnounceMediaUrl;
	}

	public String getUserData() {
		return userData;
	}

	public void setUserData(String userData) {
		this.userData = userData;
	}

	public String getSrcCallerIdName() {
		return srcCallerIdName;
	}

	public void setSrcCallerIdName(String srcCallerIdName) {
		this.srcCallerIdName = srcCallerIdName;
	}

	public String getSrcCallerIdNumber() {
		return srcCallerIdNumber;
	}

	public void setSrcCallerIdNumber(String srcCallerIdNumber) {
		this.srcCallerIdNumber = srcCallerIdNumber;
	}

}
