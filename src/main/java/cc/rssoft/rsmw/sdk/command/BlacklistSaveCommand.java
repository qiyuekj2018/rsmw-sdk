package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.bean.Blacklist;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class BlacklistSaveCommand extends AbstractApiCommand {

	private String command = "blacklistSave";

	private Blacklist blacklist = null;

	/**
	 * @param agentAid
	 * @param agentName
	 * @param agentJobNumber
	 */
	public BlacklistSaveCommand(Blacklist blacklist) {
	    this.setBlacklist(blacklist);

	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

    public Blacklist getBlacklist() {
        return blacklist;
    }

    public void setBlacklist(Blacklist blacklist) {
        this.blacklist = blacklist;
    }



}
