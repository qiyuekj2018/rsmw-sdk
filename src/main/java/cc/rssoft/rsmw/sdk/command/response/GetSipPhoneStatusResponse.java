package cc.rssoft.rsmw.sdk.command.response;

import java.util.Map;

import cc.rssoft.rsmw.sdk.command.bean.SipPhoneStatus;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class GetSipPhoneStatusResponse extends AbstractApiResponse {

	private Map<String, SipPhoneStatus> result = null;

	/**
	 * key : sipPhoneName。即sipPhone的分机号<br>
	 * value : SipPhoneStatus。即分机状态信息<br>
	 * @return result
	 */
	public Map<String, SipPhoneStatus> getResult() {
		return result;
	}

	public void setResult(Map<String, SipPhoneStatus> result) {
		this.result = result;
	}

}
