package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class ConferenceLockCommand extends AbstractApiCommand {

	private String command = "conferenceLock";

	private String exten = null;

	public ConferenceLockCommand(String exten) {
		this.exten = exten;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getExten() {
		return exten;
	}

	public void setExten(String exten) {
		this.exten = exten;
	}

}
