package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class QueueMemberUnPauseCommand extends AbstractApiCommand {

	private String command = "queueMemberUnPause";
	private String queueMemberInterface = "";
	
	/**
	 * @param queueMemberInterface
	 */
	public QueueMemberUnPauseCommand(String queueMemberInterface){
		this.queueMemberInterface = queueMemberInterface;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getQueueMemberInterface() {
		return queueMemberInterface;
	}

	public void setQueueMemberInterface(String queueMemberInterface) {
		this.queueMemberInterface = queueMemberInterface;
	}

}
