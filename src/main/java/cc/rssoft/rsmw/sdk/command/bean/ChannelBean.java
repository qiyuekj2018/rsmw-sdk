package cc.rssoft.rsmw.sdk.command.bean;

import java.util.Date;

public class ChannelBean {
	
	private Date createDate = new Date();
	private String channel;
	private String callerIdName;
	private String callerIdNumber;
	private Integer channelState;
	private String channelStateDesc;
	private String connectedLineName;
	private String connectedLineNumber;
	private String uniqueId;
	
	public String getCallerIdName() {
		return callerIdName;
	}
	public void setCallerIdName(String callerIdName) {
		this.callerIdName = callerIdName;
	}
	public String getCallerIdNumber() {
		return callerIdNumber;
	}
	public void setCallerIdNumber(String callerIdNumber) {
		this.callerIdNumber = callerIdNumber;
	}
	public Integer getChannelState() {
		return channelState;
	}
	public void setChannelState(Integer channelState) {
		this.channelState = channelState;
	}
	public String getChannelStateDesc() {
		return channelStateDesc;
	}
	public void setChannelStateDesc(String channelStateDesc) {
		this.channelStateDesc = channelStateDesc;
	}
	public String getConnectedLineName() {
		return connectedLineName;
	}
	public void setConnectedLineName(String connectedLineName) {
		this.connectedLineName = connectedLineName;
	}
	public String getConnectedLineNumber() {
		return connectedLineNumber;
	}
	public void setConnectedLineNumber(String connectedLineNumber) {
		this.connectedLineNumber = connectedLineNumber;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
}
