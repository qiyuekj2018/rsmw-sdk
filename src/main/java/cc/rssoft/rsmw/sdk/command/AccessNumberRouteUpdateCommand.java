package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.bean.AccessNumberRoute;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class AccessNumberRouteUpdateCommand extends AbstractApiCommand {

	private String command = "accessNumberRouteUpdate";

	private AccessNumberRoute accessNumberRoute = null;

	public AccessNumberRouteUpdateCommand(AccessNumberRoute accessNumberRoute) {
	    this.setAccessNumberRoute(accessNumberRoute);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}



	public AccessNumberRoute getAccessNumberRoute() {
		return accessNumberRoute;
	}

	public void setAccessNumberRoute(AccessNumberRoute accessNumberRoute) {
		this.accessNumberRoute = accessNumberRoute;
	}



}
