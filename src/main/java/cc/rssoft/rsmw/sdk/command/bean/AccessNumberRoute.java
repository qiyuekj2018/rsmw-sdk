package cc.rssoft.rsmw.sdk.command.bean;

import java.util.Date;


/**
 * 
 * 接入号路由
 * 
 * 
 * @author jiang
 *
 */

public class AccessNumberRoute  {
    
	private Long id = null;
	
	/**
	 * 网关名称
	 */
	private String gatewayName = "";
	
	/**
	 * 顺序
	 */
	private Long seq = null;
	/**
	 * 此条呼入路由的名称
	 */
	private String name = "";
	/**
	 * 此条路由对应的接入号,特殊值【*】代表【所有】
	 */
	private String accessNumber = "";
	/**
	 * 有效期开始日期
	 */
	private Date startDate = new Date(0);
	/**
	 * 有效期结束日期
	 */
	private Date endDate = new Date(0);
	/**
	 * 时间匹配策略的类型
	 */
	private String periodType = ""; //ALL,直接匹配所有天和时段；2,DAY所有天；3,DAY_AND_HOUR，详见PeriodType
	
	/**
	 * 一周当中的天，以逗号隔开，1代表周日，7代表周六
	 */
	private String periodDayInWeek = "";
	/**
	 * 一天当中的时段
	 */
	private String periodStartTime = "";
	/**
	 * 一天当中的时段
	 */
	private String periodEndTime = "";
	
	/**
	 * 目标路由的exten类型
	 */
	private String destType = "";
	/**
	 * 目标路由的具体exten
	 */
	private String dest = "";
	
	/**
	 * 目标接入号
	 */
	private String dstAccessNumber = "";
	
	/**
	 * 目标网关
	 */
	private String dstGateway = "";
	
	/**
	 * 是否启用“最近联系”功能
	 */
	private Boolean enableRecentContact = false;
	/**
	 * 定义多少天算“最近”
	 */
	private Integer recentContactDays = 30;
	
	public Long getSeq() {
		return seq;
	}
	public void setSeq(Long seq) {
		this.seq = seq;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAccessNumber() {
		return accessNumber;
	}
	public void setAccessNumber(String accessNumber) {
		this.accessNumber = accessNumber;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getPeriodType() {
		return periodType;
	}
	public void setPeriodType(String periodType) {
		this.periodType = periodType;
	}
	public String getPeriodDayInWeek() {
		return periodDayInWeek;
	}
	public void setPeriodDayInWeek(String periodDayInWeek) {
		this.periodDayInWeek = periodDayInWeek;
	}
	public String getPeriodStartTime() {
		return periodStartTime;
	}
	public void setPeriodStartTime(String periodStartTime) {
		this.periodStartTime = periodStartTime;
	}
	public String getPeriodEndTime() {
		return periodEndTime;
	}
	public void setPeriodEndTime(String periodEndTime) {
		this.periodEndTime = periodEndTime;
	}
	public String getDestType() {
		return destType;
	}
	public void setDestType(String destType) {
		this.destType = destType;
	}
	public String getDest() {
		return dest;
	}
	public void setDest(String dest) {
		this.dest = dest;
	}
	public String getDstAccessNumber() {
		return dstAccessNumber;
	}
	public void setDstAccessNumber(String dstAccessNumber) {
		this.dstAccessNumber = dstAccessNumber;
	}
	public String getDstGateway() {
		return dstGateway;
	}
	public void setDstGateway(String dstGateway) {
		this.dstGateway = dstGateway;
	}
    public Boolean getEnableRecentContact() {
        return enableRecentContact;
    }
    public void setEnableRecentContact(Boolean enableRecentContact) {
        this.enableRecentContact = enableRecentContact;
    }
    public Integer getRecentContactDays() {
        return recentContactDays;
    }
    public void setRecentContactDays(Integer recentContactDays) {
        this.recentContactDays = recentContactDays;
    }
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
    public String getGatewayName() {
        return gatewayName;
    }
    public void setGatewayName(String gatewayName) {
        this.gatewayName = gatewayName;
    }

}
