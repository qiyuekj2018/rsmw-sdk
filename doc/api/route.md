# 7. API Route

## 7.1 说明

【API 路由】用来让第三方应用决定呼入通话的路由：RSMW会在有电话呼入时向第三方URL发起请求，告知关键参数。第三方应用可以通过这些参数结合自身的业务数据来决定将该通呼叫路由到哪里。
“API路由”功能在“呼入路由”或“IVR”中进行配置

## 7.2 请求

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| channel | 通道名称 | String | 64 | 是 |
| src | 主叫 | String | 64 | 是 |
| srcAccessNumber | 主叫接入号 | String | 64 | |
| ivrExten | 当前IVR分机号 | String | 64 | |
| ivrKeyPressed | IVR中的按键 | String | 64 | ||

- Request:

```json
    {
        "timestamp":1478321775260,
        "event":"apiRoute",
        "channel":"SIP/8002-00001111",
        "src":"13391026171",
        "srcAccessNumber":"60172133",
        "ivrExten":"6001",
        "ivrKeyPressed":"654321",
        "callUuid":"3abdca2d71bb47af834390ec29032be3"
    }
``` 

## 7.3 响应

第三方给出响应，用于决定该通呼叫应该被路由到哪里

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| dst | 目标号码（希望把该通电话路由到哪里去。可以路由到内部Exten、自定义Dialplan的Exten或外部号码） | String | 64 | 是 |
| dstGateway | 目标网关（如果dst是外部号码，则需要填写呼出时用的网关） | String | 64 | 是 |
| dstAccessNumber | 目标接入号（如果dst需要从网关呼出，则用该号码作为主叫） | String | 64 | ||

路由到音乐

- Response:

```json
    {
        "timestamp":1478321775260,
        "errCode":"0",
        "dst":""
    }
```  

路由到内部分机

- Response:

```json
    {
        "timestamp":1478321775260,
        "errCode":"0",
        "dst":"8001"
    }
```  

路由到外部号码

- Response:

```
    {
        "timestamp":1478321775260,
        "errCode":"0",
        "dst":"13391026171",
        "dstGateway":"gw1",
        "dstAccessNumber":"60172133"
    }
```  