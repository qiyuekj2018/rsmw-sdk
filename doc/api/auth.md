# 3. API认证

## 3.1 签名

RSMW和第三方系统之间通过“校验签名”的方法验证对方的身份。

当你的应用需要向RSMW服务器端发送jsonString时，先获整个jsonString的sign：

    String sign = RsmwSignUtil.sign(jsonString, apiSecret);

然后把sign在URL中带上，供rsmw服务器端校验：

    String url = "http://192.168.100.100/rsmw/api/2.0?sign=" + sign;

```
    http://192.168.100.100/rsmw/api/2.0?sign=371d58f95967f0c87b95376b39ec2552

    {
        "timestamp":"1478321782349",
        "command":"orignate", 
        "xxxx":"yyyy"
    }
```

同样，RSMW向你的应用发送Event时，也会带上sign

## 3.2 签名代码

Java签名工具类：

```java

    import java.security.MessageDigest;

    public class RsmwSignUtil {
        private static final char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

        private static final String md5(String s) {
            try {
                byte[] btInput = s.getBytes();
                // 获得MD5摘要算法的 MessageDigest 对象
                MessageDigest mdInst = MessageDigest.getInstance("MD5");
                // 使用指定的字节更新摘要
                mdInst.update(btInput);
                // 获得密文
                byte[] md = mdInst.digest();
                // 把密文转换成十六进制的字符串形式
                int j = md.length;
                char str[] = new char[j * 2];
                int k = 0;
                for (int i = 0; i < j; i++) {
                    byte byte0 = md[i];
                    str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                    str[k++] = hexDigits[byte0 & 0xf];
                }
                return new String(str);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }    
        
        public static final String sign(String jsonString, String apiSecret){
            String result = md5(jsonString + apiSecret);
            return result;
        }
    }
```