# 附录2：调试工具

我们推荐使用Chrome或Firefox上的HTTP插件：

Chrome上的Postman：[postman](http://www.getpostman.com/)

Firefox上的REST Client：[REST Client](https://addons.mozilla.org/zh-CN/firefox/addon/restclient/)

他们会简化你的调试工作。

当然，你完全可以直接写代码调试