# 4. Quick Start

## 4.1 API 配置
- URL：第三方系统通过此URL来向RSMW发送命令
- 启用sign校验：是否对第三方系统的身份进行认证
- apiSecret：启用sign校验后，双方计算sign的秘钥
- 回调URL：RSMW通过此URL向第三方系统推送Event
- API EVENT：启用您希望订阅的EVENT

## 4.2 测试

* 测试调用：

通过你自己的代码，向RSMW发送

```json
    {
        "timestamp":"1478321782349",
        "command":"helloWorld"
    }
```

RSMW 会向你返回

```json
    {
        "timestamp":1478321782349,
        "errCode":"0",
        "errMsg":"Hello World!"
    }
```

* 测试回调：

进入到页面，点击“测试”按钮。回调URL会收页面“请求(request)”处填写的内容，并将URL给出的返回显示在“响应(response)”处

## 4.3 订阅Event

勾选需要订阅的Event